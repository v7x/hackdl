package application

import (
	"fmt"
	"gitlab.com/v7x/hackdl/bitcount"
	"gitlab.com/v7x/hackdl/util"
)

/*
   GosperLoopDetect analyzes a function which returns an int32 and returns the
   lower and upper bounds of the function's output and the period over which
   the function does not repeat. The function is named after the man who
   discovered it, R. W. Gosper.
*/
func GosperLoopDetect(f func(x int32) int32, X0 int32, mulow, muup, lambda *int32) {
	var n int
	var T [33]int32

	T[0] = X0
	Xn := X0
	for n = 1; ; n++ {
		Xn = f(Xn)
		kmax := 31 - bitcount.NLZ(n) //Floor(log2(n))
		for k := 0; k <= kmax; k++ {
			if Xn == T[k] {
				goto L
			}
		}
		T[bitcount.NTZ(n+1)] = Xn // no match
	}

L:
	//Compute m = max{i | i < n and ntz(i+1) = k}
	m := ((((n >> k) - 1) | 1) << k) - 1
	lambda = &(n - m)
	lgl := 31 - bitcount.NLZ(*lambda-1)            //Ceil(log2(lambda)) - 1
	muup = &m                                      //upper bound
	mulow = &(m - util.Max(1, 1<<lgl).(int32) + 1) //lower bound
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
        case int:
        case int8:
        case int16:
        case int32:
        case int64:
        case uint:
        case uint8:
        case uint16:
        case uint32:
        case uint64:
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
                u := y.(int)
        case int8:
                u := y.(int8)
        case int16:
                u := y.(int16)
        case int32:
                u := y.(int32)
        case int64:
                u := y.(int64)
        case uint:
                u := y.(uint)
        case uint8:
                u := y.(uint8)
        case uint16:
                u := y.(uint16)
        case uint32:
                u := y.(uint32)
        case uint64:
                u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
