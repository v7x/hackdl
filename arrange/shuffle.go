package arrange

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   TODO: For all shuffle functions, find algorithms for integer sizes other
   than 32 bits.
*/

/*
   OuterPerfectShuffle interleaves bits in two halves of an integer.
   With letters representing each bit, the word on the first line
   would return the word on the second:

   1) abcd efgh ijkl mnop ABCD EFGH IJKL MNOP

   2) aAbB cCdD eEfF gGhH iIjJ kKlL mMnN oOpP
*/
func OuterPerfectShuffle(x interface{}) interface{} {
	switch t := x.(type) {
	case int:

	case int8:

	case int16:

	case int32:
		u := (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		return t
	case int64:

	case uint:

	case uint8:

	case uint16:

	case uint32:
		u := (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		return t
	case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   OuterUnshuffle reverses the effect of the outer perfect shuffle.
   Interestingly, for 32-bit integers it requires performing the same
   instructions as the initial shuffle in reverse order.
*/
func OuterUnshuffle(x interface{}) interface{} {
	switch t := x.(type) {
	case int:

	case int8:

	case int16:

	case int32:
		u := (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		return t
	case int64:

	case uint:

	case uint8:

	case uint16:

	case uint32:
		u := (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		return t
	case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   InnerPerfectShuffle interleaves bits in two halves of an integer.
   With letters representing each bit, the word on the first line
   would return the word on the second:

   1) abcd efgh ijkl mnop ABCD EFGH IJKL MNOP

   2) AaBb CcDd EeFf GgHh IiJj KkLl MmNn OoPp

   The instruction performed to achieve this are exactly the same as the outer
   shuffle, except they are prepended by swapping the first and second half of
   the int.
*/
func InnerPerfectShuffle(x interface{}) interface{} {
	switch t := x.(type) {
	case int:

	case int8:

	case int16:

	case int32:
		t = (t >> 16) | (t << 16)
		u := (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		return t
	case int64:

	case uint:

	case uint8:

	case uint16:

	case uint32:
		t = (t >> 16) | (t << 16)
		u := (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		return t
	case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   InnerUnshuffle reverses the effect of the outer perfect shuffle.
   Interestingly, for 32-bit integers it requires performing the same
   instructions as the initial shuffle in reverse order.
*/
func InnerUnshuffle(x interface{}) interface{} {
	switch t := x.(type) {
	case int:

	case int8:

	case int16:

	case int32:
		t = (t >> 16) | (t << 16)
		u := (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		return t
	case int64:

	case uint:

	case uint8:

	case uint16:

	case uint32:
		t = (t >> 16) | (t << 16)
		u := (t ^ (t >> 1)) & 0x22222222
		t = t ^ u ^ (u << 1)
		u = (t ^ (t >> 2)) & 0x0C0C0C0C
		t = t ^ u ^ (u << 2)
		u = (t ^ (t >> 4)) & 0x00F000F0
		t = t ^ u ^ (u << 4)
		u = (t ^ (t >> 8)) & 0x0000FF00
		t = t ^ u ^ (u << 8)
		return t
	case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   OuterHalfShuffle performs an outer shuffle on a 32-bit word that is only
   using the 16 least signifigant bits. With letters representing the used
   bits, the word on the first line returns the word on the second:

   1) 0000 0000 0000 0000 ABCD EFGH IJKL MNOP

   2) 0A0B 0C0D 0E0F 0G0H 0I0J 0K0L 0M0N 0O0P

   The function uses less instructions than the original function, so it
   is desirable for 32-bit integers ranging from 0 to 65535 (although this
   function is useless for 0, because it returns 0).
*/
func OuterHalfShuffle(x int32) int32 {
	x = ((x & 0xFF00) << 8) | (x & 0x00FF)
	x = ((x << 4) | x) & 0x0F0F0F0F
	x = ((x << 2) | x) & 0x33333333
	return ((x << 1) | x) & 0x55555555
}

func OuterHalfUnshuffle(x int32) int32 {
	//x &= 0x55555555 //this could be used to clear odd bits
	x = ((x >> 1) | x) & 0x33333333
	x = ((x >> 2) | x) & 0x0F0F0F0F
	x = ((x >> 4) | x) & 0x00FF00FF
	return ((x << 8) | x) & 0x0000FFFF
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
