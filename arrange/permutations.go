package arrange

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   SheepsAndGoats compresses all the 1 bits in a mask to the left of x,
   and all the 0 bits in a mask to the right of x. Also known as "generalized
   unshuffle".
*/
func SheepAndGoats(x, y uint32) uint32 {
	return CompressLeft(x, y) | Compress(x, ^y)
}

/*
   Permutate takes an integer x and an array of integers p. Each element
   of p makes up a 32xN bit matrix representing positions of where N is the
   number of modifications made to x in an arbitrary operation. For example,
   a rotate shift left of 4 positions moves the first bit to position 4, the
   second to position 5, and so on until th thirty-second bit is moved to
   postion 3. This permutation can be represented as a vector of 32 5-bit
   indecies. Each element of p is used then as a column in that matrix, so the
   most significant bit in each element would make up row 1, and the least
   significant bits would make up row 32. Each bit of p[0] is the least
   significant bit of each row, and each bit of p[4] is the most significant.
*/
func Permutate(x uint32, p []uint32) uint32 {
	ln := len(p)
	for i := 0; i < ln; i++ {
		x = SheepsAndGoats(x, p[i])
		for j := i; j < ln-1; j++ {
			p[j] = SheepsAndGoats(p[j], p[i])
		}
	}
	return x
}

/*
   unrolledPermutate demonstrates an unrolling of the loop for the
   permutation described as the example in the description of Permutation().
   This could be done for any array p of arbitrary length provided you know
   len(p) ahead of time, as it is more efficient than looping.
*/
/*
func unrolledPermutate(x uint32, p []uint32) uint32 {
	x = SheepAndGoats(x, p[0])
	p[1] = SheepAndGoats(p[1], p[0])
	p[2] = SheepAndGoats(p[2], p[0])
	p[3] = SheepAndGoats(p[3], p[0])
	p[4] = SheepAndGoats(p[4], p[0])

	x = SheepAndGoats(x, p[1])
	p[2] = SheepAndGoats(p[2], p[1])
	p[3] = SheepAndGoats(p[3], p[1])
	p[4] = SheepAndGoats(p[4], p[1])

	x = SheepAndGoats(x, p[2])
	p[3] = SheepAndGoats(p[3], p[2])
	p[4] = SheepAndGoats(p[4], p[2])

	x = SheepAndGoats(x, p[3])
	p[4] = SheepAndGoats(p[4], p[3])

	x = SheepAndGoats(x, p[4])

	return x
}
*/

/*
   Permutations is a more efficient implementation of Permutate if your
   matrix array p will be used on multiple values for x. It does this by
   calculating the values p will use beforehand. You could also unroll
   the the first loop (including the inner part) and the loop inside the
   second if you know the appropriate values ahead of time.
*/
func Permutations(x []uint32, p []uint32) {
	for i, v := range p {
		for _, w := range p[i:] {
			v = SheepsAndGoats(v, w)
		}
		p[i] = v
	}

	for i, v := range x {
		for _, w := range p {
			v = SheepAndGoats(v, w)
		}
		x[i] = v
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
