package arrange

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
)

/*
   BitReverse reverse the bits in an integer (ex: BitReverse(1011) -> 1101)
   TODO: calculate values for other bit widths.
*/
func BitReverse(i interface{}) interface{} {
	switch x := i.(type) {
	case int:
		if util.IntSize == 32 {
			return BitReverse(int32(x))
		} else {
			return BitReverse(int64(x))
		}
	case int8:

	case int16:

	case int32:
		x = (x << 15) | (x >> 17)
		t := (x ^ (x >> 16)) & 0x003F801F
		x = (t | (t << 10)) ^ x
		t := (x ^ (x >> 4)) & 0x0E038421
		x = (t | (t << 4)) ^ x
		t := (x ^ (x >> 2)) & 0x22488842
		x = (t | (t << 2)) ^ x
		return x
	case int64:
		x = (x << 31) | (x >> 33)
		t := (x ^ (x >> 20)) & 0x00000FFF800007FF
		x = (t | (t << 20)) ^ x
		t := (x ^ (x >> 8)) & 0x00F8000F80700807
		x = (t | (t << 8)) ^ x
		t := (x ^ (x >> 4)) & 0x0808708080807008
		x = (t | (t << 4)) ^ x
		t := (x ^ (x >> 2)) & 0x1111111111111111
		x = (t | (t << 2)) ^ x
		return x
	case uint:
		if util.IntSize == 32 {
			return BitReverse(uint32(x))
		} else {
			return BitReverse(uint64(x))
		}
	case uint8:

	case uint16:

	case uint32:
		x = (x << 15) | (x >> 17)
		t := (x ^ (x >> 16)) & 0x003F801F
		x = (t | (t << 10)) ^ x
		t := (x ^ (x >> 4)) & 0x0E038421
		x = (t | (t << 4)) ^ x
		t := (x ^ (x >> 2)) & 0x22488842
		x = (t | (t << 2)) ^ x
		return x
	case uint64:
		x = (x << 31) | (x >> 33)
		t := (x ^ (x >> 20)) & 0x00000FFF800007FF
		x = (t | (t << 20)) ^ x
		t := (x ^ (x >> 8)) & 0x00F8000F80700807
		x = (t | (t << 8)) ^ x
		t := (x ^ (x >> 4)) & 0x0808708080807008
		x = (t | (t << 4)) ^ x
		t := (x ^ (x >> 2)) & 0x1111111111111111
		x = (t | (t << 2)) ^ x
		return x
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   RevIncrement increase x by 1 and its reverse by reversed 1 (as in
   BitReverse(x + 1))
*/
func RevIncrement(x interface{}) (interface{}, interface{}) {
	switch t := x.(type) {
	case int:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case int8:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case int16:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case int32:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case int64:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case uint:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case uint8:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case uint16:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case uint32:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	case uint64:
		r := BitReverse(t).(int)
		m := ^0
		i := t ^ (t + 1) + 1
		r ^= m - (^m / i)
		return r, t + 1
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
