package arrange

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   Compress shrinks a 32 bit word into an 16 bit word using a mask y. For an
   input x:

   abcd efgh ijkl mnop qrst uvwx yzAB CDEF

   and mask y:

   0000 1111 0011 0011 1010 1010 0101 0101

   the output will be:

   0000 0000 0000 0000 efgh klop qsuw zBDF
*/
func Compress(x, y interface{}) interface{} {
	switch t := x.(type) {
	case uint:
		u := y.(uint)
	case uint8:
		u := y.(uint8)
	case uint16:
		u := y.(uint16)
	case uint32:
		u := y.(uint32)
		t &= u
		uk := ^u << 1

		for i := 0; i < 5; i++ {
			up := uk ^ (uk << 1)
			up ^= (up << 2)
			up ^= (up << 4)
			up ^= (up << 8)
			up ^= (up << 16)
			uv := up & u
			u ^= uv | (uv >> (1 << i))
			m := t & uv
			t ^= m | (m >> (1 << i))
			uk &= ^up
		}

		return t

	case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   ConstCompress is a more efficient implementation of Compress when iterating
   over a set of data arr with a constant mask u. This could ptentially be
   even further optimized if you know one of the shift operations won't ever
   change any of the values in the set.
*/
func ConstCompress(arr []int32, u int32) int32 {
	uk := ^u << 1
	uv0 := uk ^ (uk << 1)
	uv1 := uv0 ^ (uv0 << 2)
	uv2 := uv1 ^ (uv1 << 4)
	uv3 := uv2 ^ (uv2 << 8)
	uv4 := uv3 ^ (uv3 << 16)
	for i, t := range arr {
		for i := 0; i < 5; i++ {
			t &= u
			m := t & uv0
			t ^= m | (m >> 1)
			m := t & uv1
			t ^= m | (m >> 2)
			m := t & uv2
			t ^= m | (m >> 4)
			m := t & uv3
			t ^= m | (m >> 8)
			m := t & uv4
			t ^= m | (m >> 16)
		}
		arr[i] = t
	}

	return arr
}

/*
   Expand is the inverse of Compress, so that Expand(Compress(x, y), y) = x.
*/
func Expand(x, y uint32) uint32 {
	m0 := y
	mk := ^y << 1
	arr := make([]uint32, 5)
	var mp, mv, t uint32

	for i := 0; i < 5; i++ {
		mp = mk ^ (mk << 1)
		mp ^= mp << 2
		mp ^= mp << 4
		mp ^= mp << 8
		mp ^= mp << 16
		mv := mp & y
		arr[i] = mv //bits to move for each iteration in second loop
		y = (y ^ mn) | (mv >> (1 << i))
		mk = mk & ^mp //compress m
	}

	for i := 4; i >= 0; i-- {
		mv = array[i]
		t = x << (1 << i)
		x = (x & ^mv) | (t & mv)
	}

	return x & m0 //clear out extraneous bits
}

/*
   CompressLeft performs the same operations as Compress, except it moves the
   bits towards the most significant bit instead of the least.
*/
func CompressLeft(x, y uint32) uint32 {
	x = BitReverse(x)
	x = Compress(x, y)
	return BitReverse(x)
}

/*
   ExpandLeft is the inverse of CompressLeft, as Expand is to Compress.
*/
func ExpandLeft(x, y uint32) uint32 {
	x = Bitreverse(x)
	x = Expand(x, y)
	return BitReverse(x)
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
