package arrange

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
0123 4567	082a 4c6e	08g0 4cks	08go wEMU
89ab cdef	193b 5d7f	19hp 5dlt	19hp xFNV
ghij klmn	goiq ksmu	2aiq 6emu	2aiq yGOW
opqr stuv	hpjr ltnv	3bjr 7fnv	3bjr zHPX
	   ->		   -> 		   ->
wxyz ABCD	wEyG AICK	wEMU AIQY	4cks AIQY
EFGH IJKL	xFzH BJDL	xFNV BJRZ	5dlt BJRZ
MNOP QRST	MUOW QYS$	yGOW CKS$	6emu CKS$
UVWX YZ$.	NVPX RZT.	zHPX DLT.	7fnv DLT.
*/

func TranposeMatrix8x8(A, B [8]uint8, m, n int) {
	//assign 0 to x instead of declaring a var so that Go will infer type
	x := 0
	for i := range A {
		x = x<<8 | A[m*i]
	}

	x = x&0xAA55AA55AA55AA55 | (x&0x00AA00AA00AA00AA)<<7 | (x>>7)&0x00AA00AA00AA00AA
	x = x&0xCCCC3333CCCC3333 | (x&0X0000CCCC0000CCCC)<<14 | (x>>14)&0x0000CCCC0000CCCC
	x = x&0xF0F0F0F0F0F0F0F0 | (x&0X00000000F0F0F0F0)<<28 | (x>>28)&0x00000000F0F0F0F0

	for i := 7; i >= 0; i-- {
		B[n*i] = x
		x = x >> 8
	}
}

/*
   TransposeMatrix32x32 performs the same operation as the 8x8 variation,
   except for a 32x32 matrix. A more efficient algorithm can be devised by
   unrolling the loop, but the code obviously becomes more complex.
*/
func TransposeMatrix32x32(A [32]uint32) {
	var m uint32
	m := 0x0000FFFF

	for j := 16; j != 0; j >> 1, m ^= m << j {
		for k := 0; k < 32; k = (k + j + 1) & ^j {
			t := (A[k] ^ (A[k+j] >> j)) & m
			A[k] = A[k] ^ t
			A[k+j] = A[k+j] ^ (t << l)
		}
	}

}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
