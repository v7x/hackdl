package wordsearch

import (
	"fmt"
	"gitlab.com/v7x/hackdl/bitcount"
	"gitlab.com/v7x/hackdl/util"
)

/*
   The following functions LeftmostZeroByte and RightmostZeroByte each search
   for the first zero-byte in a rune starting from the left or right. If one
   xor's the argument r with a rune in which each byte is the desired value.
*/

/*
   LeftmostZeroByte finds the first 8-bit string of zeros starting from the
   least significant bit of a rune. The function returns 0 to 3 if one of
   bytes zero to three are the 0-byte, and 4 if there is no 0-byte.
*/
func LeftmostZeroByte(r rune) int {
	switch {
	case r>>24 == 0:
		return 0
	case r&0x00FF0000 == 0:
		return 1
	case r&0x0000FF00 == 0:
		return 2
	case r&0x000000FF == 0:
		return 3
	default:
		return 4
	}
}

/*
   RoghtmostZeroByte finds the first 8-bit string of zeros starting from the
   most significant bit of a rune. The function returns 0 to 3 if one of the
   bytes zero to three are the 0-byte, and 4 if there is no 0-byte.
*/
func RightmostZeroByte(r rune) int {
	y := (r - 0x01010101) & ^r & 0x80808080
	return bitcount.NTZ(y) >> 3
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
