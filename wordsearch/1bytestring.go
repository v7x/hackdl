package wordsearch

import (
	"fmt"
	"gitlab.com/v7x/hackdl/bitcount"
	"gitlab.com/v7x/hackdl/util"
)

/*
   First1bitStr finds the first string of 1-bits in an integer of length n.
   The main loop may become expensive for larer values of n, so an alternative
   is provided for 64-bit integers. (The alternative algorithm works for other
   integer sizes as well.) For those who desire to further optimize the
   algorithm, it may be desirable to unroll the loop for the appropriate number
   of times based on integer size.
*/
func First1bitStr(x interface{}, n int) int {
	switch t := x.(type) {
	case int:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return util.IntSize
	case int8:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return 8
	case int16:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return 16
	case int32:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return 32
	case int64:
		for n > 1 {
			s := n >> 1
			t &= t >> s
			n -= s
		}
		return bitcount.NLZ(t)
	case uint:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return util.IntSize
	case uint8:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return 8
	case uint16:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return 16
	case uint32:
		p := 0
		for t != 0 {
			k := bitcount.NLZ(t)
			t <<= k
			p += k
			k = bitcount.NLZ(^t)
			if k >= n {
				return p
			}
			t <<= k
			p += k
		}
		return 32
	case uint64:
		for n > 1 {
			s := n >> 1
			t &= t >> s
			n -= s
		}
		return bitcount.NLZ(t)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Longest1bitStr finds the longest consecutive string of 1-bits in an
   integer. An alternative implementation that is more performant is available
   in section 3 of chapter 6  of Hacker's Delight. However it requires heavy
   use of goto and was decided against simply for readability.
*/
func Longest1bitStr(x interface{}) int {
	switch t := x.(type) {
	case int:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case int8:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case int16:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case int32:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case int64:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case uint:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case uint8:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case uint16:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case uint32:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	case uint64:
		var k int

		for k = 0; t != 0; k++ {
			t &= 2 * t
		}
		return k
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Shortest1bitStr finds the shortest 1-bit string in x starting at position
   *apos.
*/
func Shortest1bitStr(x interface{}, apos *int) interface{} {
	var k int

	switch t := x.(type) {
	case int:
		if x == 0 {
			apos = &util.IntSize
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case int8:
		if x == 0 {
			apos = &8
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case int16:
		if x == 0 {
			apos = &16
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case int32:
		if x == 0 {
			apos = &32
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case int64:
		if x == 0 {
			apos = &64
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case uint:
		if x == 0 {
			apos = &util.IntSize
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case uint8:
		if x == 0 {
			apos = &8
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case uint16:
		if x == 0 {
			apos = &16
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case uint32:
		if x == 0 {
			apos = &32
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	case uint64:
		if x == 0 {
			apos = &64
			return 0
		}
		b := ^(t >> 1) & t
		e := t & ^(t << 1)
		for k = 1; (b & e) == 0; k++ {
			e <<= 1
		}
		apos = &bitcount.NLZ(b & e)
		return k
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
