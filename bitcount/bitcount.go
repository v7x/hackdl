package bitcount

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
)

/*
   PopCount, or Population Count, counts the number of 1-bits in an integer.
   Use this over the Count1Bits* functions if you don't know your numberset
   ahead of time.
   TODO: Calculate values for integers of other sizes than 32.
*/
func PopCount(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		return PopCount(uint(t))
	case int8:
		return PopCount(uint8(t))
	case int16:
		return PopCount(uint16(t))
	case int32:
		return PopCount(uint32(t))
	case int64:
		return PopCount(uint64(t))
	case uint:
		sum := int(t)
		for i := 1; i <= util.IntShift; i++ {
			t = util.LeftRShift(t, 1).(uint)
			sum += int(t)
		}
		return -sum
	case uint8:
		sum := int8(t)
		for i := 1; i <= 7; i++ {
			t = util.LeftRShift(t, 1).(uint8)
			sum += int8(t)
		}
		return -sum
	case uint16:
		sum := int16(t)
		for i := 1; i <= 15; i++ {
			t = util.LeftRShift(t, 1).(uint16)
			sum += int16(t)
		}
		return -sum
	case uint32:
		//0x55555555 binary:
		//01010101010101010101010101010101
		t -= (t >> 1) & 0x55555555
		//0x33333333 binary:
		//00110011001100110011001100110011
		t = (t & 0x33333333) + ((t >> 2) & 0x33333333)
		//0x0F0F0F0F binary:
		//00001111000011110000111100001111
		t = (t + (t >> 4)) & 0x0F0F0F0F
		t += (t >> 8)
		t += (t >> 16)
		return x & 0x0000003F //63
	case uint64:
		sum := int64(t)
		for i := 1; i <= 63; i++ {
			t = util.LeftRShift(t, 1).(uint64)
			sum += int64(t)
		}
		return -sum
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Count1BitsLow counts the number of 1 bits int an integer by continuously
   turning off the right most 1-bit in x until x is 0. Works for all sizes
   of integers. Works faster with lower numbers of 1 bits. If you know your
   numberset has mostly low numbers of 1's, use this equation. Otherwise
   use Count1BitsHigh or PopulationCount.
*/
func Count1BitsLow(x interface{}) int {
	switch t := x.(type) {
	case int:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case int8:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case int16:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case int32:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case int64:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case uint:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case uint8:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case uint16:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case uint32:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	case uint64:
		n := 0
		for t != 0 {
			n += 1
			t &= (t - 1)
		}
		return n
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Count1Bits high works similar to Count1BitsLow, except it flips every 0 to
   1 instead of the reverse. Works best on values with a high number of
   1-bits. If you know your numberset has mostly numbers with high numbers
   of 1's, use this. Otherwise, use Count1BitsLow or PopCount.
*/
func Count1BitsHigh(x interface{}) int {
	switch t := x.(type) {
	case int:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return util.IntSize - n
	case int8:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 8 - n
	case int16:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 16 - n
	case int32:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 32 - n
	case int64:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 64 - n
	case uint:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return util.IntSize - n
	case uint8:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 8 - n
	case uint16:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 16 - n
	case uint32:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 32 - n
	case uint64:
		n := 0
		for t != (^0) {
			n += 1
			t |= (t + 1)
		}
		return 64 - n
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   PopCmp returns the integer between x and y that has the larger population
   count. The or loop will run at most s/2 times where s is the size of the
   integer being used, since we clear 1-bits at the same position before
   the loop The most bits two integers can have with no overlap is half the
   size of s, which becomes the maximum number of loops.
*/
func PopCmp(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		v := t & ^u
		z := u & ^t
		for true {
			if v == 0 {
				return z & -z
			}
			if z == 0 {
				return 1
			}
			v &= (v - 1)
			z &= (z - 1)
		}
	case int8:
		u := y.(int8)
		v := t & ^u
		z := u & ^t
		for true {
			if v == 0 {
				return z & -z
			}
			if z == 0 {
				return 1
			}
			v &= (v - 1)
			z &= (z - 1)
		}
	case int16:
		u := y.(int16)
		v := t & ^u
		z := u & ^t
		for true {
			if v == 0 {
				return z & -z
			}
			if z == 0 {
				return 1
			}
			v &= (v - 1)
			z &= (z - 1)
		}
	case int32:
		u := y.(int32)
		v := t & ^u
		z := u & ^t
		for true {
			if v == 0 {
				return z & -z
			}
			if z == 0 {
				return 1
			}
			v &= (v - 1)
			z &= (z - 1)
		}
	case int64:
		u := y.(int64)
		v := t & ^u
		z := u & ^t
		for true {
			if v == 0 {
				return z & -z
			}
			if z == 0 {
				return 1
			}
			v &= (v - 1)
			z &= (z - 1)
		}
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NLZ calculates the number of leading 0-bits in an integer. There are many
   ways this algorithm can be implemented, the methods below account for only
   a few. See section 3 of chapter 5 of Hacker's Delight for other
   implementations.
*/
func NLZ(x interface{}) int {
	switch t := x.(type) {
	case int:
		if util.IntSize == 32 {
			return NLZ(int32(t))
		} else {
			return NLZ(int64(t))
		}
	case int8:
		if t == 0 {
			return 8
		}
		n := 1
		if t>>4 == 0 {
			n += 4
			t <<= 4
		}
		if t>>6 == 0 {
			n += 2
			t <<= 2
		}
		n -= (t >> 7)
		return n
	case int16:
		if t == 0 {
			return 16
		}
		n := 1
		if t>>8 == 0 {
			n += 8
			t <<= 8
		}
		if t>>12 == 0 {
			n += 4
			t <<= 4
		}
		if t>>14 == 0 {
			n += 2
			t <<= 2
		}
		n -= (t >> 15)
		return n
	case int32:
		if t == 0 {
			return 32
		}
		n := 1
		if t>>16 == 0 {
			n += 16
			t <<= 16
		}
		if t>>24 == 0 {
			n += 8
			t <<= 8
		}
		if t>>28 == 0 {
			n += 4
			t <<= 4
		}
		if t>>30 == 0 {
			n += 2
			t <<= 2
		}
		n -= t >> 31
		return n
	case int64:
		if t == 0 {
			return 64
		}
		n := 1
		if t>>32 == 0 {
			n += 32
			t <<= 32
		}
		if t>>48 == 0 {
			n += 16
			t <<= 16
		}
		if t>>56 == 0 {
			n += 8
			t <<= 8
		}
		if t>>60 == 0 {
			n += 4
			t <<= 4
		}
		if t>>62 == 0 {
			n += 4
			t <<= 4
		}
		n -= t >> 63
		return n
	case uint:
		if util.IntSize == 32 {
			return NLZ(uint32(t))
		} else {
			return NLZ(uint64(t))
		}
	case uint8:
		if t == 0 {
			return 8
		}
		n := 1
		if t>>4 == 0 {
			n += 4
			t <<= 4
		}
		if t>>6 == 0 {
			n += 2
			t <<= 2
		}
		n -= (t >> 7)
		return n
	case uint16:
		if t == 0 {
			return 16
		}
		n := 1
		if t>>8 == 0 {
			n += 8
			t <<= 8
		}
		if t>>12 == 0 {
			n += 4
			t <<= 4
		}
		if t>>14 == 0 {
			n += 2
			t <<= 2
		}
		n -= (t >> 15)
		return n
	case uint32:
		if t == 0 {
			return 32
		}
		n := 1
		if t>>16 == 0 {
			n += 16
			t <<= 16
		}
		if t>>24 == 0 {
			n += 8
			t <<= 8
		}
		if t>>28 == 0 {
			n += 4
			t <<= 4
		}
		if t>>30 == 0 {
			n += 2
			t <<= 2
		}
		n -= t >> 31
		return n
	case uint64:
		if t == 0 {
			return 64
		}
		n := 1
		if t>>32 == 0 {
			n += 32
			t <<= 32
		}
		if t>>48 == 0 {
			n += 16
			t <<= 16
		}
		if t>>56 == 0 {
			n += 8
			t <<= 8
		}
		if t>>60 == 0 {
			n += 4
			t <<= 4
		}
		if t>>62 == 0 {
			n += 4
			t <<= 4
		}
		n -= t >> 63
		return n
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NLZEQ returns true if x and y have the same number of leading 0's, and
   false otherwise.
*/
func NLZEQ(x, y interface{}) bool {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return (t ^ u) <= (t & u)
	case int8:
		u := y.(int8)
		return (t ^ u) <= (t & u)
	case int16:
		u := y.(int16)
		return (t ^ u) <= (t & u)
	case int32:
		u := y.(int32)
		return (t ^ u) <= (t & u)
	case int64:
		u := y.(int64)
		return (t ^ u) <= (t & u)
	case uint:
		u := y.(uint)
		return (t ^ u) <= (t & u)
	case uint8:
		u := y.(uint8)
		return (t ^ u) <= (t & u)
	case uint16:
		u := y.(uint16)
		return (t ^ u) <= (t & u)
	case uint32:
		u := y.(uint32)
		return (t ^ u) <= (t & u)
	case uint64:
		u := y.(uint64)
		return (t ^ u) <= (t & u)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NLZLT returns true if x has less leading zeros than y, and false otherwise.
*/
func NLZLT(x, y interface{}) bool {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return (t & ^u) > u
	case int8:
		u := y.(int8)
		return (t & ^u) > u
	case int16:
		u := y.(int16)
		return (t & ^u) > u
	case int32:
		u := y.(int32)
		return (t & ^u) > u
	case int64:
		u := y.(int64)
		return (t & ^u) > u
	case uint:
		u := y.(uint)
		return (t & ^u) > u
	case uint8:
		u := y.(uint8)
		return (t & ^u) > u
	case uint16:
		u := y.(uint16)
		return (t & ^u) > u
	case uint32:
		u := y.(uint32)
		return (t & ^u) > u
	case uint64:
		u := y.(uint64)
		return (t & ^u) > u
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NLZLE returns true if x has less than or equal to the number of leading
   zeros that y does, and false otherwise.
*/
func NLZLE(x, y interface{}) bool {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return (u % ^t) <= t
	case int8:
		u := y.(int8)
		return (u % ^t) <= t
	case int16:
		u := y.(int16)
		return (u % ^t) <= t
	case int32:
		u := y.(int32)
		return (u % ^t) <= t
	case int64:
		u := y.(int64)
		return (u % ^t) <= t
	case uint:
		u := y.(uint)
		return (u % ^t) <= t
	case uint8:
		u := y.(uint8)
		return (u % ^t) <= t
	case uint16:
		u := y.(uint16)
		return (u % ^t) <= t
	case uint32:
		u := y.(uint32)
		return (u % ^t) <= t
	case uint64:
		u := y.(uint64)
		return (u % ^t) <= t
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func NLZGT(x, y interface{}) bool {
	return NLZLT(y, x)
}

func NLZGE(x, y interface{}) bool {
	return NLZLE(y, x)
}

/*
   Log2 finds the integer log base 2 of x. It may seem odd that it is located
   in the bitcount package, but it is actually just the NLZ function
   subtracted from the size of the integer minus 1.
*/
func Log2(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		return util.IntShift - NLZ(t).(int)
	case int8:
		return 7 - NLZ(t).(int8)
	case int16:
		return 15 - NLZ(t).(int16)
	case int32:
		return 31 - NLZ(t).(int32)
	case int64:
		return 63 - NLZ(t).(int64)
	case uint:
		return util.IntShift - NLZ(t).(uint)
	case uint8:
		return 7 - NLZ(t).(uint8)
	case uint16:
		return 15 - NLZ(t).(uint16)
	case uint32:
		return 31 - NLZ(t).(uint32)
	case uint64:
		return 63 - NLZ(t).(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Bitsize returns the minimum number of bits required to represent a number
   in two's complement.
*/
func Bitsize(x interface{}) int {
	switch t := x.(type) {
	case int:
		t ^= (t >> util.IntShift)
		return util.IntSize + 1 - NLZ(t)
	case int8:
		t ^= (t >> 7)
		return 9 - NLZ(t)
	case int16:
		t ^= (t >> 15)
		return 17 - NLZ(t)
	case int32:
		t ^= (t >> 31)
		return 33 - NLZ(t)
	case int64:
		t ^= (t >> 63)
		return 65 - NLZ(t)
	case uint:
		t ^= (t >> util.IntShift)
		return util.IntSize + 1 - NLZ(t)
	case uint8:
		t ^= (t >> 7)
		return 9 - NLZ(t)
	case uint16:
		t ^= (t >> 15)
		return 17 - NLZ(t)
	case uint32:
		t ^= (t >> 31)
		return 33 - NLZ(t)
	case uint64:
		t ^= (t >> 63)
		return 65 - NLZ(t)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NTZ, or Number of Trailing Zeros, calculates the number of tailing zeros
   after the final 1-bit in an integer. There are quite a few different
   implementations available to use in the calculation. See section 4 of
   chapter 5 in Hacker's Delight for more examples.
*/
func NTZ(x interface{}) int {
	switch t := x.(type) {
	case int:
		return util.IntSize - NLZ(^t&(t-1))
	case int8:
		return 8 - NLZ(^t&(t-1))
	case int16:
		return 16 - NLZ(^t&(t-1))
	case int32:
		return 32 - NLZ(^t&(t-1))
	case int64:
		return 64 - NLZ(^t&(t-1))
	case uint:
		return util.IntSize - NLZ(^t&(t-1))
	case uint8:
		return 8 - NLZ(^t&(t-1))
	case uint16:
		return 16 - NLZ(^t&(t-1))
	case uint32:
		return 32 - NLZ(^t&(t-1))
	case uint64:
		return 64 - NLZ(^t&(t-1))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Nfact2 calculates the number of factors of 2 in x. If x is 0,
   it returns -1. It may seem odd to include this in the bitcount package,
   but the calculation is similar to NTZ() above.
*/
func Nfact2(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		return util.IntShift - NLZ(t&-t)
	case int8:
		return 7 - NLZ(t&-t)
	case int16:
		return 15 - NLZ(t&-t)
	case int32:
		return 31 - NLZ(t&-t)
	case int64:
		return 63 - NLZ(t&-t)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(uint)
        case int8:
		u := y.(uint8)
        case int16:
		u := y.(uint16)
        case int32:
		u := y.(uint32)
        case int64:
		u := y.(uint64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
