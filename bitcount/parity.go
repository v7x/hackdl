package bitcount

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   Parity returns 1 if an integer has an odd number of 1-bits and 0 otherwise.
*/
func Parity(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		//this performs the same instructions as the other equations,
		//but allows for the max right shift length to be dynamic
		//based on platform integer size.
		p := t ^ (t >> 1)
		for i := 2; i <= util.IntSize/2; i *= 2 {
			p ^= (p >> i)
		}
		return p
	case int8:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		return p
	case int16:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		p ^= (p >> 8)
		return p
	case int32:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		p ^= (p >> 8)
		p ^= (p >> 16)
		return p
	case int64:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		p ^= (p >> 8)
		p ^= (p >> 16)
		p ^= (p >> 32)
		return p
	case uint:
		//this performs the same instructions as the other equations,
		//but allows for the max right shift length to be dynamic
		//based on platform integer size.
		p := t ^ (t >> 1)
		for i := 2; i <= util.IntSize/2; i *= 2 {
			p ^= (p >> i)
		}
		return p
	case uint8:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		return p
	case uint16:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		p ^= (p >> 8)
		return p
	case uint32:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		p ^= (p >> 8)
		p ^= (p >> 16)
		return p
	case uint64:
		p := t ^ (t >> 1)
		p ^= (p >> 2)
		p ^= (p >> 4)
		p ^= (p >> 8)
		p ^= (p >> 16)
		p ^= (p >> 32)
		return p
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   ParityBit applies a parity bit to a 7-bit integer. 7-bit integers range from
   -64 to 63. This is not a very efficient way to do things unless you are
   limited in memory, as a lookup table would be more effective. However it
   is a novel concept, so it is included.
*/
func ParityBit(x int32) int32 {
	return ((x * 0x10204081) & 0x888888FF) % 1920
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(uint)
        case int8:
		u := y.(uint8)
        case int16:
		u := y.(uint16)
        case int32:
		u := y.(uint32)
        case int64:
		u := y.(uint64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
