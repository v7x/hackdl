package main

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   Base -2, or "negabinary", is derived from dividing a number by 2 and
   recording the remainders. For example, to find the representation of -3,
   -3/-2 = 2rem1
   2/-2 = -1rem0
   1/-2 = 1rem1
   1/-2 = 0rem1
   Now that we have a 0 quotient, we are finished. Reading the remainders from
   the bottom up, we find the negabinary representation of -3 is 1101.

*/

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
	case int8:
		u := y.(int8)
	case int16:
		u := y.(int16)
	case int32:
		u := y.(int32)
	case int64:
		u := y.(int64)
	case uint:
		u := y.(uint)
	case uint8:
		u := y.(uint8)
	case uint16:
		u := y.(uint16)
	case uint32:
		u := y.(uint32)
	case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
