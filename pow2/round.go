package pow2

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
	"math/bits"
)

/*
   RoundTwd0Pow rounds x towards 0 to the next multiple of n.
*/
func RoundTwd0Pos(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		s := (t >> util.IntShift) & u
		return (t + s) & -(u)
	case int8:
		u := y.(int8)
		s := (t >> 7) & u
		return (t + s) & -(u)
	case int16:
		u := y.(int16)
		s := (t >> 15) & u
		return (t + s) & -(u)
	case int32:
		u := y.(int32)
		s := (t >> 31) & u
		return (t + s) & -(u)
	case int64:
		u := y.(int64)
		s := (t >> 63) & u
		return (t + s) & -(u)
	case uint:
		u := y.(uint)
		return t & -(u)
	case uint8:
		u := y.(uint8)
		return t & -(u)
	case uint16:
		u := y.(uint16)
		return t & -(u)
	case uint32:
		u := y.(uint32)
		return t & -(u)
	case uint64:
		u := y.(uint64)
		return t & -(u)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func RoundAwy0Pow(x, y interface{}) interface{} {
	switch t := x.(type) {
	case uint:
		u := y.(int)
		return t + uint(-int(t)&(util.Sign(u).(int)*util.Abs(u).(int)-1))
	case uint8:
		u := y.(int8)
		return t + uint8(-int8(t)&(util.Sign(u).(int8)*util.Abs(u).(int8)-1))
	case uint16:
		u := y.(int16)
		return t + uint16(-int16(t)&(util.Sign(u).(int16)*util.Abs(u).(int16)-1))
	case uint32:
		u := y.(int32)
		return t + uint32(-int32(t)&(util.Sign(u).(int32)*util.Abs(u).(int32)-1))
	case uint64:
		u := y.(int64)
		return t + uint64(-int64(t)&(util.Sign(u).(int64)*util.Abs(u).(int64)-1))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NextPowerUp finds the next power of y in the positive direction.
*/
func NextPowerUp(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		//v is abs(u) - 1 with the sign of u
		v := util.Sign(u).(int) * (util.Abs(u).(int) - 1)
		z := -u
		return (t + v) & z
	case int8:
		u := y.(int8)
		//v is abs(u) - 1 with the sign of u
		v := util.Sign(u).(int8) * (util.Abs(u).(int8) - 1)
		z := -u
		return (t + v) & z
	case int16:
		u := y.(int16)
		//v is abs(u) - 1 with the sign of u
		v := util.Sign(u).(int16) * (util.Abs(u).(int16) - 1)
		z := -u
		return (t + v) & z
	case int32:
		u := y.(int32)
		//v is abs(u) - 1 with the sign of u
		v := util.Sign(u).(int32) * (util.Abs(u).(int32) - 1)
		z := -u
		return (t + v) & z
	case int64:
		u := y.(int64)
		//v is abs(u) - 1 with the sign of u
		v := util.Sign(u).(int64) * (util.Abs(u).(int64) - 1)
		z := -u
		return (t + v) & z
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NextPowerDown finds the next power of y in the negative direction.
*/
func NextPowerDown(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return t & -u
	case int8:
		u := y.(int8)
		return t & -u
	case int16:
		u := y.(int16)
		return t & -u
	case int32:
		u := y.(int32)
		return t & -u
	case int64:
		u := y.(int64)
		return t & -u
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Flp2 (Floor power of 2) finds the next power of 2 less than x.
*/
func Flp2(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(util.IntShift-bits.LeadingZeros(uint(t)))
	case int8:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(7-bits.LeadingZeros8(uint8(t)))
	case int16:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(15-bits.LeadingZeros16(uint16(t)))
	case int32:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(31-bits.LeadingZeros32(uint32(t)))
	case int64:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(63-bits.LeadingZeros64(uint64(t)))
	case uint:
		return 1 << uint(util.IntShift-bits.LeadingZeros(t))
	case uint8:
		return 1 << uint(7-bits.LeadingZeros8(t))
	case uint16:
		return 1 << uint(15-bits.LeadingZeros16(t))
	case uint32:
		return 1 << uint(31-bits.LeadingZeros32(t))
	case uint64:
		return 1 << uint(63-bits.LeadingZeros64(t))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Clp2 (Ceiling power of 2) finds the next power of 2 greater than x.
*/
func Clp2(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(util.IntShift-bits.LeadingZeros(uint(t)))
	case int8:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(7-bits.LeadingZeros8(uint8(t)))

	case int16:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(15-bits.LeadingZeros16(uint16(t)))
	case int32:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(31-bits.LeadingZeros32(uint32(t)))
	case int64:
		if t < 0 {
			panic("Cannot find next power of 2 when x is negative.")
		}
		return 1 << uint(63-bits.LeadingZeros64(uint64(t)))
	case uint:
		return 1 << uint(util.IntShift-bits.LeadingZeros(t))
	case uint8:
		return 1 << uint(7-bits.LeadingZeros8(t))
	case uint16:
		return 1 << uint(15-bits.LeadingZeros16(t))
	case uint32:
		return 1 << uint(31-bits.LeadingZeros32(t))
	case uint64:
		return 1 << uint(63-bits.LeadingZeros64(t))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   CeilPow2 provides an alternative to the algorithm for Clp2 if one cannot
   afford the type conversions required for the LeadingZeros* functions.
*/
func CeilPow2(x int) int {
	x -= 1
	x |= x >> 1
	x |= x >> 2
	x |= x >> 4
	x |= x >> 8
	x |= x >> 16
	return x + 1
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(uint)
        case int8:
		u := y.(uint8)
        case int16:
		u := y.(uint16)
        case int32:
		u := y.(uint32)
        case int64:
		u := y.(uint64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
