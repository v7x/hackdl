package pow2

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
)

/*
   BoundCrossed returns True if a uint of varying size crosses a boundary
   determined by 'len', which is a power of 2. Returns false otherwise.
   Note that in len == 1, a boundary crossing will never occur.
*/
func BoundCrossed(x, len interface{}) bool {
	var in bool
	check := len.(uint64)
	pow2 := Pow2()
	for i := range pow2 {
		if check == pow2[i] {
			in = true
		}
	}
	if !in {
		panic("Length of boundary must be a power of 2 between 2^0 and 2^63.")
	}
	switch t := x.(type) {
	case uint:
		u := len.(uint)
		return util.IntSize-(t&util.IntShift) < u
	case uint8:
		u := len.(uint8)
		return 8-(t&7) < u
	case uint16:
		u := len.(uint16)
		return 16-(t&15) < u
	case uint32:
		u := len.(uint32)
		return 32-(t&31) < u
	case uint64:
		u := len.(uint64)
		return 64-(t&63) < u
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(uint)
        case int8:
		u := y.(uint8)
        case int16:
		u := y.(uint16)
        case int32:
		u := y.(uint32)
        case int64:
		u := y.(uint64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
