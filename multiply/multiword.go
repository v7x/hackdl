package multiply

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   MultiWord (written as multiply.MultiWord in practice) multiplies two
   multiword integers (made up uf halfword "digits") and returns the product.
   It uses the "grade-school" method of multiplication to acomplish this.
*/
func MultiWord(x, y []int16) []int16 {
	xl := len(x)
	yl := len(y)

	z := make([]int16, xl+yl)

	for j := 0; j < yl; j++ {
		k := 0
		for i := 0; i < m; i++ {
			t := x[i]*y[j] + z[i+j] + k
			w[i+j] = t
			k = t >> 16
		}
		w[j+xl] = k
	}

	if x[xl-1].(int8) < 0 {
		b := 0
		for j := 0; j < n; j++ {
			t := z[j+xl] - y[j] - b
			z[j+xl] = t
			b = t >> 31
		}
	}

	if y[yl-1].(int8) < 0 {
		b := 0
		for i := 0; i < m; i++ {
			t := z[i+yl] - x[i] - b
			w[i+yl] = t
			b = t >> 31
		}
	}

	return z
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
