package multiply

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
)

/*
   HighOrderHalf computes the high-order 32 or 64 bits of the product of
   two 32-bit or 64-bit integers, respectively.
*/
func HighOrderHalf(x, y interface{}) interface{} {
	switch u := x.(type) {
	case int:
		v := y.(int)
		if util.IntSize == 32 {
			return MulHS(int32(x), int32(y))
		} else {
			return MulHS(int64(x), int64(y))
		}
	case int32:
		v := y.(int32)

		u0 := u & 0xFFFF
		u1 := u >> 16
		v0 := v & 0xFFFF
		v1 := v >> 16
		w0 := u0 * v0
		t := u1*v0 + (w0 >> 16)

		w1 := t & 0xFFF
		w2 := t >> 16
		w1 = u0*v1 + w1
		return u1*v1 + w2 + (w1 >> 16)
	case int64:
		v := y.(int64)

		u0 := u & 0xFFFFFFFF
		u1 := u >> 32
		v0 := v & 0xFFFFFFFF
		v1 := v >> 32
		w0 := u0 * v0
		t := u1*v0 + (w0 >> 32)

		w1 := t & 0xFFFFFFFF
		w2 := t >> 32
		w1 = u0*v1 + w1
		return u1*v1 + w2 + (w1 >> 32)
	case uint:
		v := y.(uint)
		if util.IntSize == 32 {
			return MulHS(uint32(x), uint32(y))
		} else {
			return MulHS(uint64(x), uint64(y))
		}
	case uint32:
		v := y.(uint32)
		u0 := u & 0xFFFF
		u1 := u >> 16
		v0 := v & 0xFFFF
		v1 := v >> 16
		w0 := u0 * v0
		t := u1*v0 + (w0 >> 16)

		w1 := t & 0xFFF
		w2 := t >> 16
		w1 = u0*v1 + w1
		return u1*v1 + w2 + (w1 >> 16)
	case uint64:
		v := y.(uint64)
		u0 := u & 0xFFFF
		u1 := u >> 16
		v0 := v & 0xFFFF
		v1 := v >> 16
		w0 := u0 * v0
		t := u1*v0 + (w0 >> 16)

		w1 := t & 0xFFF
		w2 := t >> 16
		w1 = u0*v1 + w1
		return u1*v1 + w2 + (w1 >> 16)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
