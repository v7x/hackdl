package util

import (
	"fmt"
)

/*
   LeftRShift performs a left rotate shift on x, moving n bits.
*/
func LeftRShift(x, n interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		s := n.(uint8)
		if (0 > s) || (s > 8) {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (8 - s))
	case int16:
		s := n.(uint16)
		if (0 > s) || (s > 16) {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (16 - s))
	case int:
		s := n.(uint)
		if (0 > s) || (s > IntSize) {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (IntSize - s))
	case int32:
		s := n.(uint32)
		if (0 > s) || (s > 32) {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (32 - s))
	case int64:
		s := n.(uint64)
		if (0 > s) || (s > 64) {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (64 - s))
	case uint8:
		s := n.(uint8)
		if s > 8 {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (8 - s))
	case uint16:
		s := n.(uint16)
		if s > 16 {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (16 - s))
	case uint:
		s := n.(uint)
		if s > IntSize {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (IntSize - s))
	case uint32:
		s := n.(uint32)
		if s > 32 {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (32 - s))
	case uint64:
		s := n.(uint64)
		if s > 64 {
			panic(fmt.Sprintf("Invalid shift lenght %v", s))
		}
		return (t << s) | (t >> (64 - s))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   RightRShift performs a right rotate shift on x, moving n bits.
*/
func RightRShift(x, n interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		s := n.(uint8)
		if (0 > s) || (s > 8) {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (8 - s))
	case int16:
		s := n.(uint16)
		if (0 > s) || (s > 16) {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (16 - s))
	case int:
		s := n.(uint)
		if (0 > s) || (s > IntSize) {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (IntSize - s))
	case int32:
		s := n.(uint32)
		if (0 > s) || (s > 32) {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (32 - s))
	case int64:
		s := n.(uint64)
		if (0 > s) || (s > 64) {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (64 - s))
	case uint8:
		s := n.(uint8)
		if s > 8 {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (8 - s))
	case uint16:
		s := n.(uint16)
		if s > 16 {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (16 - s))
	case uint:
		s := n.(uint)
		if s > IntSize {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (IntSize - s))
	case uint32:
		s := n.(uint32)
		if s > 32 {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (32 - s))
	case uint64:
		s := n.(uint64)
		if s > 64 {
			panic(fmt.Sprintf("Invalid shift lenght %v", n))
		}
		return (t >> s) | (t << (64 - s))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	t1 := x.(type)
	t2, ok := y.(t1)

	if !(ok) {
		panic(fmt.Sprintf("%T is not %T.", y, x))
	}

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
