package util

import (
	"fmt"
)

/*
   Sign returns -1 if x < 0, 1 if x > 0, and 0 otherwise.
*/
func Sign(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return IntBool(t > 0) - IntBool(t < 0)
	case int8:
		return IntBool(t > 0) - IntBool(t < 0)
	case int16:
		return IntBool(t > 0) - IntBool(t < 0)
	case int32:
		return IntBool(t > 0) - IntBool(t < 0)
	case int64:
		return IntBool(t > 0) - IntBool(t < 0)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   SignShift performs a shift with the opposite sign of its opperands.
*/
func SignShift(x, y interface{}) interface{} {

	switch t := x.(type) {
	case int:
		u := y.(uint)
		return int(uint(t) >> u)
	case int8:
		u := y.(uint8)
		return int8(uint8(t) >> u)
	case int16:
		u := y.(uint16)
		return int16(uint16(t) >> u)
	case int32:
		u := y.(uint32)
		return int32(uint32(t) >> u)
	case int64:
		u := y.(uint64)
		return int64(uint64(t) >> u)
	case uint:
		u := y.(uint)
		return uint(int(t) >> u)
	case uint8:
		u := y.(uint8)
		return uint8(int8(t) >> u)
	case uint16:
		u := y.(uint16)
		return uint16(int16(t) >> u)
	case uint32:
		u := y.(uint32)
		return uint32(int32(t) >> u)
	case uint64:
		u := y.(uint64)
		return uint64(int64(t) >> u)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   TransferSign returns ISIGN(x,y). It should be used when working with those
   unfamiliar with Fortran.
*/
func TransferSign(x, y interface{}) interface{} {
	return ISIGN(x, y)
}

/*
   ISIGN transfers y's signage to x.
*/
func ISIGN(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		s := y.(int8) >> 7
		return (Abs(t).(int8) ^ s) - s
	case int16:
		s := y.(int16) >> 15
		return (Abs(t).(int16) ^ s) - s
	case int:
		s := y.(int) >> IntShift
		return (Abs(t).(int) ^ s) - s
	case int32:
		s := y.(int32) >> 31
		return (Abs(t).(int32) ^ s) - s
	case int64:
		s := y.(int64) >> 63
		return (Abs(t).(int64) ^ s) - s
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
