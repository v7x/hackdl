package util

import (
	"fmt"
)

/*
   Alt takes an integer x that is meant to contain one of two values, and
   sets it to the value it currently isn't. It is a more efficient
   implementation of "if x == a, x = b; else x = a".
*/
func Alt(x, a, b interface{}) interface{} {

	switch t := x.(type) {
	case int:
		u := a.(int)
		v := b.(int)
		t = u + v - t
		return t
	case int8:
		u := a.(int8)
		v := b.(int8)
		t = u + v - t
		return t
	case int16:
		u := a.(int16)
		v := b.(int16)
		t = u + v - t
		return t
	case int32:
		u := a.(int32)
		v := b.(int32)
		t = u + v - t
		return t
	case int64:
		u := a.(int64)
		v := b.(int64)
		t = u + v - t
		return t
	case uint:
		u := a.(uint)
		v := b.(uint)
		t = u ^ v ^ t
		return t
	case uint8:
		u := a.(uint8)
		v := b.(uint8)
		t = u ^ v ^ t
		return t
	case uint16:
		u := a.(uint16)
		v := b.(uint16)
		t = u ^ v ^ t
		return t
	case uint32:
		u := a.(uint32)
		v := b.(uint32)
		t = u ^ v ^ t
		return t
	case uint64:
		u := a.(uint64)
		v := b.(uint64)
		t = u ^ v ^ t
		return t
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   AltBool performs Alt on an integer that is either 1 or 0.
*/
func AltBool(x int) int {
	if x > 1 || x < 0 {
		panic("Argument must be equivalent to 1 or 0.")
	}
	return Alt(x, 1, 0).(int)
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	t1 := x.(type)
	t2, ok := y.(t1)

	if !(ok) {
		panic(fmt.Sprintf("%T is not %T.", y, x))
	}

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
