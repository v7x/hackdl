package util

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestAbs(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	ints := RandArray(100)
	ints8 := RandArray8(100)
	ints16 := RandArray16(100)
	ints32 := RandArray32(100)
	ints64 := RandArray64(100)

	abserrstr := "Abs(%T %v) returned %T %v. Index %v. %s"

	fmt.Println("Testing ints")
	for i, v := range ints {
		a := Abs(v).(int)
		if a < 0 {
			s := "Return less than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v < 0 && a != -v {
			s := "Val less than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v >= 0 && a != v {
			s := "Val greater than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	for i, v := range ints {
		a := Nabs(v).(int)
		if a > 0 {
			s := "Return greater than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v > 0 && a != -v {
			s := "Val greater than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v <= 0 && a != v {
			s := "Val less than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	fmt.Println("Testing int8s")
	for i, v := range ints8 {
		a := Abs(v).(int8)
		if a < 0 {
			s := "Return less than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v < 0 && a != -v {
			s := "Val less than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v >= 0 && a != v {
			s := "Val greater than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	for i, v := range ints8 {
		a := Nabs(v).(int8)
		if a > 0 {
			s := "Return greater than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v > 0 && a != -v {
			s := "Val greater than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v <= 0 && a != v {
			s := "Val less than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	fmt.Println("Testing int16s")
	for i, v := range ints16 {
		a := Abs(v).(int16)
		if a < 0 {
			s := "Return less than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v < 0 && a != -v {
			s := "Val less than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v >= 0 && a != v {
			s := "Val greater than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	for i, v := range ints16 {
		a := Nabs(v).(int16)
		if a > 0 {
			s := "Return greater than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v > 0 && a != -v {
			s := "Val greater than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v <= 0 && a != v {
			s := "Val less than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	fmt.Println("Testing int32s")
	for i, v := range ints32 {
		a := Abs(v).(int32)
		if a < 0 {
			s := "Return less than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v < 0 && a != -v {
			s := "Val less than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v >= 0 && a != v {
			s := "Val greater than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	for i, v := range ints32 {
		a := Nabs(v).(int32)
		if a > 0 {
			s := "Return greater than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v > 0 && a != -v {
			s := "Val greater than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v <= 0 && a != v {
			s := "Val less than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	fmt.Println("Testing int64s")
	for i, v := range ints64 {
		a := Abs(v).(int64)
		if a < 0 {
			s := "Return less than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v < 0 && a != -v {
			s := "Val less than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v >= 0 && a != v {
			s := "Val greater than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}

	for i, v := range ints64 {
		a := Nabs(v).(int64)
		if a > 0 {
			s := "Return greater than 0."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v > 0 && a != -v {
			s := "Val greater than 0 and return is not -(val)."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		} else if v <= 0 && a != v {
			s := "Val less than 0 but return is not val."
			t.Errorf(abserrstr, v, v, a, a, i, s)
		}
	}
}

func TestAlternate(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	xints := RandArray(100)
	yints := RandArray(100)

	for i := range xints {
		a := xints[i]
		b := yints[i]
		var x int
		if rand.Int()%2 == 1 {
			x = a
		} else {
			x = b
		}
		origin := x

		x = Alt(x, a, b).(int)

		if origin == a {
			if x != b {
				t.Errorf("Origin is a, but x is not b.")
			}
		} else if origin == b {
			if x != a {
				t.Errorf("Origin is b, but x is not a.")
			}
		} else {
			t.Errorf("Somehow the original value is not a or b. The fuck did you do?")
		}

	}
}

func TestAvg(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	xints32 := RandArray32(100)
	yints32 := RandArray32(100)

	for i := range xints32 {
		x := xints32[i]
		y := yints32[i]
		f := FloorAvg(x, y).(int32)
		c := CeilAvg(x, y).(int32)
		fmt.Printf("Floor average of %v and %v: %v\n", x, y, f)
		fmt.Printf("Ceiling average of %v and %v: %v\n", x, y, c)
	}
}

func TestCmp(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	xints := RandArray(100)
	yints := RandArray(100)
	xints = append(xints, 1)
	yints = append(yints, 1)
	xints = append(xints, ^0)
	yints = append(yints, ^0)

	for i := range xints {
		x := xints[i]
		y := yints[i]
		res := Cmp(x, y)
		if res == 1 {
			fmt.Printf("%v is greater than %v.\n", x, y)
		} else if res == 0 {
			fmt.Printf("%v and %v are equal.\n", x, y)
		} else if res == -1 {
			fmt.Printf("%v is less than %v.\n", y, y)
		} else {
			t.Errorf("Cmp(%v, %v) returned an invalid value %v.\n", x, y, res)
		}
	}

	for i := range xints {
		x := xints[i]
		y := yints[i]
		res := Max(x, y)
		fmt.Printf("%v is the max of (%v, %v).\n", res, x, y)
	}

	for i := range xints {
		x := xints[i]
		y := yints[i]
		res := Min(x, y)
		fmt.Printf("%v is the min of (%v, %v).\n", res, x, y)
	}

	fmt.Println("testing EQ")
	for i := range xints {
		x := xints[i]
		y := yints[i]
		res := EQ(x, y)
		if res == 1 {
			fmt.Printf("%v and %v are equal.\n", x, y)
		} else if res == 0 {
			fmt.Printf("%v and %v are not equal.\n", x, y)
		} else {
			t.Errorf("EQ(%v, %v) returned an invalid value %v.\n", x, y, res)
		}
	}
	fmt.Println("testing NE")
	for i := range xints {
		x := xints[i]
		y := yints[i]
		res := NE(x, y)
		if res == 1 {
			fmt.Printf("%v and %v are not equal.\n", x, y)
		} else if res == 0 {
			fmt.Printf("%v and %v are equal.\n", x, y)
		} else {
			t.Errorf("NE(%v, %v) returned an invalid value %v.\n", x, y, res)
		}
	}
	/*
		fmt.Println("testing LT")
		for i := range xints {
			x := xints[i]
			y := yints[i]
			res := NE(x, y)
			if res == 1 {
				fmt.Printf("%v is less than %v.\n", x, y)
			} else if res == 0 {
				fmt.Printf("%v is not less than %v.\n", x, y)
			} else {
				t.Errorf("LT(%v, %v) returned an invalid value %v.\n", x, y, res)
			}
		}

		fmt.Println("testing LE")
		for i := range xints {
			x := xints[i]
			y := yints[i]
			res := NE(x, y)
			fmt.Println(res)
			if res == 1 {
				fmt.Printf("%v is less than or equal to  %v.\n", x, y)
			} else if res == 0 {
				fmt.Printf("%v is not less than or equal to %v.\n", x, y)
			} else {
				t.Errorf("LT(%v, %v) returned an invalid value %v.\n", x, y, res)
			}
		}
	*/
	xuints := RandArrayU(100)
	yuints := RandArrayU(100)
	fmt.Println("testing LT")
	for i := range xuints {
		x := xuints[i]
		y := yuints[i]
		res := LT(x, y).(uint)
		if res == 1 {
			fmt.Printf("%v is less than %v.\n", x, y)
		} else if res == 0 {
			fmt.Printf("%v is not less than %v.\n", x, y)
		} else {
			t.Errorf("LT(%v, %v) returned an invalid value %v.\n", x, y, res)
		}
	}

	fmt.Println("testing LE")
	for i := range xuints {
		x := xuints[i]
		y := yuints[i]
		res := LE(x, y).(uint)
		if res == 1 {
			fmt.Printf("%v is less than %v.\n", x, y)
		} else if res == 0 {
			fmt.Printf("%v is not less than %v.\n", x, y)
		} else {
			t.Errorf("LE(%v, %v) returned an invalid value %v.\n", x, y, res)
		}
	}

	ints := RandArray(100)
	ints = append(ints, 0)

	fmt.Println("testing EQ0")
	for _, v := range ints {
		res := EQ0(v)
		if res == 1 {
			fmt.Printf("%v is equal to 0.\n", v)
		} else if res == 0 {
			fmt.Printf("%v is not equal to 0.\n", v)
		} else {
			t.Errorf("EQ0(%v) returned an invalid value %v.\n", v, res)
		}
	}

	fmt.Println("testing NE0")
	for _, v := range ints {
		res := NE0(v)
		if res == 1 {
			fmt.Printf("%v is not equal to 0.\n", v)
		} else if res == 0 {
			fmt.Printf("%v is equal to 0.\n", v)
		} else {
			t.Errorf("NE0(%v) returned an invalid value %v.\n", v, res)
		}
	}

	fmt.Println("testing LT0")
	for _, v := range ints {
		res := LT0(v)
		if res == 1 {
			fmt.Printf("%v is less than 0.\n", v)
		} else if res == 0 {
			fmt.Printf("%v is not less than 0.\n", v)
		} else {
			t.Errorf("LT0(%v) returned an invalid value %v.\n", v, res)
		}
	}

	fmt.Println("testing LE0")
	for _, v := range ints {
		res := LE0(v)
		if res == 1 {
			fmt.Printf("%v is less than or equal to 0.\n", v)
		} else if res == 0 {
			fmt.Printf("%v is not less than or equal to 0.\n", v)
		} else {
			t.Errorf("LE0(%v) returned an invalid value %v.\n", v, res)
		}
	}

	fmt.Println("testing GT0")
	for _, v := range ints {
		res := GT0(v)
		if res == 1 {
			fmt.Printf("%v is greater than 0.\n", v)
		} else if res == 0 {
			fmt.Printf("%v is not greater then 0.\n", v)
		} else {
			t.Errorf("LE0(%v) returned an invalid value %v.\n", v, res)
		}
	}

	fmt.Println("testing GE0")
	for _, v := range ints {
		res := GE0(v)
		if res == 1 {
			fmt.Printf("%v is greater than or equal to 0.\n", v)
		} else if res == 0 {
			fmt.Printf("%v is not greater then or equal to 0.\n", v)
		} else {
			t.Errorf("GE0(%v) returned an invalid value %v.\n", v, res)
		}
	}
}

/*
func TestX(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	//ints := RandArray(100)
}

func TestXY(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	//xints := RandArray(100)
	//yints := RandArray(100)
}
*/
