package util

import (
	"fmt"
)

/*
   FloorAvg finds the floor average of x and y without causing overflow.
*/
func FloorAvg(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return (t & u) + ((u ^ t) >> 1)
	case int8:
		u := y.(int8)
		return (t & u) + ((u ^ t) >> 1)
	case int16:
		u := y.(int16)
		return (t & u) + ((u ^ t) >> 1)
	case int32:
		u := y.(int32)
		return (t & u) + ((u ^ t) >> 1)
	case int64:
		u := y.(int64)
		return (t & u) + ((u ^ t) >> 1)
	case uint:
		u := y.(uint)
		return (t & u) + ((u ^ t) >> 1)
	case uint8:
		u := y.(uint8)
		return (t & u) + ((u ^ t) >> 1)
	case uint16:
		u := y.(uint16)
		return (t & u) + ((u ^ t) >> 1)
	case uint32:
		u := y.(uint32)
		return (t & u) + ((u ^ t) >> 1)
	case uint64:
		u := y.(uint64)
		return (t & u) + ((u ^ t) >> 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   CeilAvg finds the ceiling average of x and y without causing overflow.
*/
func CeilAvg(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return (t | u) - ((t ^ u) >> 1)
	case int8:
		u := y.(int8)
		return (t | u) - ((t ^ u) >> 1)
	case int16:
		u := y.(int16)
		return (t | u) - ((t ^ u) >> 1)
	case int32:
		u := y.(int32)
		return (t | u) - ((t ^ u) >> 1)
	case int64:
		u := y.(int64)
		return (t | u) - ((t ^ u) >> 1)
	case uint:
		u := y.(uint)
		return (t | u) - ((t ^ u) >> 1)
	case uint8:
		u := y.(uint8)
		return (t | u) - ((t ^ u) >> 1)
	case uint16:
		u := y.(uint16)
		return (t | u) - ((t ^ u) >> 1)
	case uint32:
		u := y.(uint32)
		return (t | u) - ((t ^ u) >> 1)
	case uint64:
		u := y.(uint64)
		return (t | u) - ((t ^ u) >> 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Avg finds the average of two integers rounded towards 0.
*/
/*
func Avg(x, y interface{}) interface{} {
	t1 := x.(type)
	t2, ok := y.(t1)

	if !(ok) {
		panic(fmt.Sprintf("%T is not %T.", y, x))
	}

	switch t := x.(type) {
	case int, int32:
		t := (x & y) + ((x ^ y) >> 1)
		return t + (int(uint(t>>31)) & (x ^ y))
	case uint, uint32:
		s := int(x ^ y)
		t := (x & y) + (uint(s >> 1))
		return t + ((t >> 31) & (x ^ y))
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
/*
func x(x, y interface{}) interface{} {
	t1 := x.(type)
	t2, ok := y.(t1)

	if !(ok) {
		panic(fmt.Sprintf("%T is not %T.", y, x))
	}

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
