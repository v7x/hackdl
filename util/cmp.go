package util

import (
	"fmt"
)

/*
   Cmp returns 1 if x > y, -1 if x < y, and 0 otherwise.
*/
func Cmp(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return IntBool(t > u) - IntBool(t < u)
	case int8:
		u := y.(int8)
		return IntBool(t > u) - IntBool(t < u)
	case int16:
		u := y.(int16)
		return IntBool(t > u) - IntBool(t < u)
	case int32:
		u := y.(int32)
		return IntBool(t > u) - IntBool(t < u)
	case int64:
		u := y.(int64)
		return IntBool(t > u) - IntBool(t < u)
	case uint:
		u := y.(uint)
		return IntBool(t > u) - IntBool(t < u)
	case uint8:
		u := y.(uint8)
		return IntBool(t > u) - IntBool(t < u)
	case uint16:
		u := y.(uint16)
		return IntBool(t > u) - IntBool(t < u)
	case uint32:
		u := y.(uint32)
		return IntBool(t > u) - IntBool(t < u)
	case uint64:
		u := y.(uint64)
		return IntBool(t > u) - IntBool(t < u)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   doz (difference or zero) returns the difference of x and y if x >= y, and 0
   otherwise. Does no type checking, so take care when using this function.

   Could also be written as:

   (x - y) & -(IntBool(x >= y))
*/
func doz(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		u := y.(int8)
		if t > u {
			return (t - u)
		}
		return 0

	case int16:
		u := y.(int16)
		if t > u {
			return (t - u)
		}
		return 0

	case int:
		u := y.(int)
		if t > u {
			return (t - u)
		}
		return 0

	case int32:
		u := y.(int32)
		if t > u {
			return (t - u)
		}
		return 0

	case int64:
		u := y.(int64)
		if t > u {
			return (t - u)
		}
		return 0

	case uint:
		u := y.(uint)
		if t > u {
			return (t - u)
		}
		return 0

	case uint8:
		u := y.(uint8)
		if t > u {
			return (t - u)
		}
		return 0

	case uint16:
		u := y.(uint16)
		if t > u {
			return (t - u)
		}
		return 0

	case uint32:
		u := y.(uint32)
		if t > u {
			return (t - u)
		}
		return 0

	case uint64:
		u := y.(uint64)
		if t > u {
			return (t - u)
		}
		return 0

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Max returns the larger of x and y.
*/
func Max(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return u + doz(t, u).(int)
	case int8:
		u := y.(int8)
		return u + doz(t, u).(int8)
	case int16:
		u := y.(int16)
		return u + doz(t, u).(int16)
	case int32:
		u := y.(int32)
		return u + doz(t, u).(int32)
	case int64:
		u := y.(int64)
		return u + doz(t, u).(int64)
	case uint:
		u := y.(uint)
		return u + doz(t, u).(uint)
	case uint8:
		u := y.(uint8)
		return u + doz(t, u).(uint8)
	case uint16:
		u := y.(uint16)
		return u + doz(t, u).(uint16)
	case uint32:
		u := y.(uint32)
		return u + doz(t, u).(uint32)
	case uint64:
		u := y.(uint64)
		return u + doz(t, u).(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Min returns the smaller of x and y.
*/
func Min(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return t - doz(t, u).(int)
	case int8:
		u := y.(int8)
		return t - doz(t, u).(int8)
	case int16:
		u := y.(int16)
		return t - doz(t, u).(int16)
	case int32:
		u := y.(int32)
		return t - doz(t, u).(int32)
	case int64:
		u := y.(int64)
		return t - doz(t, u).(int64)
	case uint:
		u := y.(uint)
		return t - doz(t, u).(uint)
	case uint8:
		u := y.(uint8)
		return t - doz(t, u).(uint8)
	case uint16:
		u := y.(uint16)
		return t - doz(t, u).(uint16)
	case uint32:
		u := y.(uint32)
		return t - doz(t, u).(uint32)
	case uint64:
		u := y.(uint64)
		return t - doz(t, u).(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   EQ returns 1 if x == y, 0 otherwise.
*/
func EQ(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		u := y.(int8)
		e := ^((t - u) | (u - t))
		return -1 * (e >> 7)
	case int16:
		u := y.(int16)
		e := ^((t - u) | (u - t))
		return -1 * (e >> 15)
	case int:
		u := y.(int)
		e := ^((t - u) | (u - t))
		return -1 * (e >> IntShift)
	case int32:
		u := y.(int32)
		e := ^((t - u) | (u - t))
		return -1 * (e >> 31)
	case int64:
		u := y.(int64)
		e := ^((t - u) | (u - t))
		return -1 * (e >> 63)
	case uint8:
		u := y.(uint8)
		e := ^((t - u) | (u - t))
		return e >> 7
	case uint16:
		u := y.(uint16)
		e := ^((t - u) | (u - t))
		return e >> 15
	case uint:
		u := y.(uint)
		e := ^((t - u) | (u - t))
		return e >> IntShift
	case uint32:
		u := y.(uint32)
		e := ^((t - u) | (u - t))
		return e >> 31
	case uint64:
		u := y.(uint64)
		e := ^((t - u) | (u - t))
		return e >> 63
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NE returns 1 if x != y, and 0 otherwise.
*/
func NE(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		u := y.(int8)
		e := (t - u) | (u - t)
		return e >> 7 * -1
	case int16:
		u := y.(int16)
		e := (t - u) | (u - t)
		return e >> 15 * -1
	case int:
		u := y.(int)
		e := (t - u) | (u - t)
		return e >> IntShift * -1
	case int32:
		u := y.(int32)
		e := (t - u) | (u - t)
		return e >> 31 * -1
	case int64:
		u := y.(int64)
		e := (t - u) | (u - t)
		return e >> 63 * -1
	case uint8:
		u := y.(uint8)
		e := (t - u) | (u - t)
		return e >> 7
	case uint16:
		u := y.(uint16)
		e := (t - u) | (u - t)
		return e >> 15
	case uint:
		u := y.(uint)
		e := (t - u) | (u - t)
		return e >> IntShift
	case uint32:
		u := y.(uint32)
		e := (t - u) | (u - t)
		return e >> 31
	case uint64:
		u := y.(uint64)
		e := (t - u) | (u - t)
		return e >> 63
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

//TODO: GET SIGNED LT/GT WORKING
/*
   LT returns 1 if x < y, and 0 otherwise.
*/
/*
func LT(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		u := y.(int8)
		e := Nabs(t - u).(int8)
		return e >> 7
	case int16:
		u := y.(int16)
		e := Nabs(t - u).(int16)
		return e >> 15
	case int:
		u := y.(int)
		e := Nabs(t - u).(int)
		return e >> IntShift
	case int32:
		u := y.(int32)
		e := Nabs(t - u).(int32)
		return e >> 31
	case int64:
		u := y.(int64)
		e := Nabs(t - u).(int64)
		return e >> 63
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
/*
   LE returns 1 if x <= y, and 0 otherwise.
*/
/*
func LE(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		u := y.(int8)
		e := (t | ^u) & ((t ^ u) & ((t - u) ^ t))
		return (e >> 7)
	case int16:
		u := y.(int16)
		e := (t | ^u) & ((t ^ u) & ((t - u) ^ t))
		return (e >> 15)
	case int:
		u := y.(int)
		e := (t | ^u) & ((t ^ u) & ((t - u) ^ t))
		return (e >> IntShift)
	case int32:
		u := y.(int32)
		e := (t | ^u) & ((t ^ u) & ((t - u) ^ t))
		return (e >> 31)
	case int64:
		u := y.(int64)
		e := (t | ^u) & ((t ^ u) & ((t - u) ^ t))
		return (e >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
/*
   GT returns 1 if x > y, and 0 otherwise. Implemented as LT(y, x)
*/
/*
func GT(x, y interface{}) interface{} {
	return LT(y, x)
}
*/
/*
   GT returns 1 if x > y, and 0 otherwise. Implemented as LT(y, x)
*/
/*
func GE(x, y interface{}) interface{} {
	return LE(y, x)
}
*/

/*
   LT returns 1 if x < y, and 0 otherwise.
*/
func LT(x, y interface{}) interface{} {
	switch t := x.(type) {
	case uint8:
		u := y.(uint8)
		e := ((^t) & u) | (((^t) | u) & (t - u))
		return e >> 7
	case uint16:
		u := y.(uint16)
		e := ((^t) & u) | (((^t) | u) & (t - u))
		return e >> 15
	case uint:
		u := y.(uint)
		e := ((^t) & u) | (((^t) | u) & (t - u))
		return e >> IntShift
	case uint32:
		u := y.(uint32)
		e := ((^t) & u) | (((^t) | u) & (t - u))
		return e >> 31
	case uint64:
		u := y.(uint64)
		e := ((^t) & u) | (((^t) | u) & (t - u))
		return e >> 64
	//case int, int8, int16, int32, int64:
	//requires unsigned integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   LE returns 1 i x <= y, and 0 otherwise.
*/
func LE(x, y interface{}) interface{} {
	switch t := x.(type) {
	case uint8:
		u := y.(uint8)
		e := ((^t) | u) & ((t ^ u) | ^(u - t))
		return e >> 7
	case uint16:
		u := y.(uint16)
		e := ((^t) | u) & ((t ^ u) | ^(u - t))
		return e >> 15
	case uint:
		u := y.(uint)
		e := ((^t) | u) & ((t ^ u) | ^(u - t))
		return e >> IntShift
	case uint32:
		u := y.(uint32)
		e := ((^t) | u) & ((t ^ u) | ^(u - t))
		return e >> 31
	case uint64:
		u := y.(uint64)
		e := ((^t) | u) & ((t ^ u) | ^(u - t))
		return e >> 64
	//case int, int8, int16, int32, int64:
	//requires unsigned integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   GT returns 1 if x > y, and 0 otherwise. Implemented as ULT(y, x)
*/
func GT(x, y interface{}) interface{} {
	return LT(y, x)
}

/*
   GE returns 1 if x >= y, and 0 otherwise. Implemented as UGE(y, x)
*/
func GE(x, y interface{}) interface{} {
	return LE(y, x)
}

/*
   Comparisons to 0 can be done in fewer instructions than the comparison
   functions above. Most notable are LT0, which takes a single right shift
   by the integer width minus 1; and GE0, which returns ^x.
*/

/*
   EQ0 returns 1 if x == 0, and 0 otherwise.
*/
func EQ0(x interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		e := (^t) & (t - 1)
		return int8(uint8(e) >> 7)
	case int16:
		e := (^t) & (t - 1)
		return int16(uint16(e) >> 15)
	case int:
		e := Abs(t).(int) - 1
		return int(uint(e) >> IntShift)
	case int32:
		e := (^t) & (t - 1)
		return int32(uint32(e) >> 31)
	case int64:
		e := (^t) & (t - 1)
		return int64(uint64(e) >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   NE0 returns 1 if x == 0, and 0 otherwise.
*/
func NE0(x interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		e := t | -(t)
		return -1 * (e >> 7)
	case int16:
		e := t | -(t)
		return -1 * (e >> 15)
	case int:
		e := t | -(t)
		return -1 * (e >> IntShift)
	case int32:
		e := t | -(t)
		return -1 * (e >> 31)
	case int64:
		e := t | -(t)
		return -1 * (e >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   LT0 returns 1 if x < 0, and 0 otherwise.
*/
func LT0(x interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		return -1 * (t >> 7)
	case int16:
		return -1 * (t >> 15)
	case int:
		return -1 * (t >> IntShift)
	case int32:
		return -1 * (t >> 31)
	case int64:
		return -1 * (t >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   LE0 returns 1 if x <= 0, and 0 otherwise.
*/
func LE0(x interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		e := t | (t - 1)
		return -1 * (e >> 7)
	case int16:
		e := t | (t - 1)
		return -1 * (e >> 15)
	case int:
		e := t | (t - 1)
		return -1 * (e >> IntShift)
	case int32:
		e := t | (t - 1)
		return -1 * (e >> 31)
	case int64:
		e := t | (t - 1)
		return -1 * (e >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   GT0 returns 1 if x > 0, and 0 otherwise.
*/
func GT0(x interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		e := -(t) & ^t
		return -1 * (e >> 7)
	case int16:
		e := -(t) & ^t
		return -1 * (e >> 16)
	case int:
		e := -(t) & ^t
		return -1 * (e >> IntShift)
	case int32:
		e := -(t) & ^t
		return -1 * (e >> 31)
	case int64:
		e := -(t) & ^t
		return -1 * (e >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   GE0 returns 1 if x >= 0, and 0 otherwise.
*/
func GE0(x interface{}) interface{} {
	switch t := x.(type) {
	case int8:
		return -1 * (^t >> 7)
	case int16:
		return -1 * (^t >> 16)
	case int:
		return -1 * (^t >> IntShift)
	case int32:
		return -1 * (^t >> 31)
	case int64:
		return -1 * (^t >> 63)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed integer
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int8:

	case int16:

	case int:

	case int32:

	case int64:

	//requires signed integer
	//case uint:

	//case uint8:

	//case uint16:

	//case uint32:

	//case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
