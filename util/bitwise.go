package util

import (
	"fmt"
	//"gitlab.com/v7x/hackdl/util"
)

/*
   BitwiseEQ returns a bitwise equivalence on x and y.
*/
func BitwiseEQ(x, y interface{}) interface{} {
	switch t := x.(type) {
	case int:
		u := y.(int)
		return ^(t ^ u)
	case int8:
		u := y.(int8)
		return ^(t ^ u)
	case int16:
		u := y.(int16)
		return ^(t ^ u)
	case int32:
		u := y.(int32)
		return ^(t ^ u)
	case int64:
		u := y.(int64)
		return ^(t ^ u)
	case uint:
		u := y.(uint)
		return ^(t ^ u)
	case uint8:
		u := y.(uint8)
		return ^(t ^ u)
	case uint16:
		u := y.(uint16)
		return ^(t ^ u)
	case uint32:
		u := y.(uint32)
		return ^(t ^ u)
	case uint64:
		u := y.(uint64)
		return ^(t ^ u)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
