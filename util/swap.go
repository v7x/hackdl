package util

/*
   Swap swaps 2 variable's values without using another integer. In Go this
   can also be achieved by using a function F(x, y) that returns y, x; or with
   the statement "x, y = y, x". The function here is for novelty's sake as
   well as to document the technique.

   There exist a few other similar methods as well, such as:

   x = x - y
   y = y + x
   x = y - x

   and:

   x = y - x
   y = y - x
   x = x + y

   and:

   x = x + y
   y = x - y
   x = x - y
*/
func Swap(x, y int) (int, int) {
	x = x ^ y
	y = y ^ x
	x = x ^ y
	return x, y
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	t1 := x.(type)
	t2, ok := y.(t1)

	if !(ok) {
		panic(fmt.Sprintf("%T is not %T.", y, x))
	}

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
