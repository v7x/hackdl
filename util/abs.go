package util

import (
	"fmt"
)

/*
   Abs finds the absolute value of x. May or may not be faster than the builtin
   abs(), it is reccomended you use that instead. This is mostly here to
   compliment Nabs() below.

   The return statements of each case can also be written as:

   (x + y) ^ y

   or:

   x - ((2 * x) & y)
*/
func Abs(x interface{}) interface{} {

	switch t := x.(type) {
	case int8:
		y := t >> 7
		return (t ^ y) - y
	case int16:
		y := t >> 15
		return (t ^ y) - y
	case int:
		y := t >> IntShift
		return (t ^ y) - y
	case int32:
		y := t >> 31
		return (t ^ y) - y
	case int64:
		y := t >> 63
		return (t ^ y) - y
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed int
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Nabs finds the negative absolute value of x.

   The return statement of each case can also be written as:

   (y - x) ^ y

   or:

   ((2 * x) & y) - x
*/
func Nabs(x interface{}) interface{} {

	switch t := x.(type) {
	case int8:
		y := t >> 7
		return y - (t ^ y)
	case int16:
		y := t >> 15
		return y - (t ^ y)
	case int:
		y := t >> IntShift
		return y - (t ^ y)
	case int32:
		y := t >> 31
		return y - (t ^ y)
	case int64:
		y := t >> 63
		return y - (t ^ y)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed int
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}
*/
