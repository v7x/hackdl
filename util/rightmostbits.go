package util

import (
	"fmt"
)

/*
   TurnOffRightmost1 turns off the rightmost 1-bit in an int to 0.
*/
func TurnOffRightMost1(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t & (t - 1)
	case int8:
		return t & (t - 1)
	case int16:
		return t & (t - 1)
	case int32:
		return t & (t - 1)
	case int64:
		return t & (t - 1)
	case uint:
		return t & (t - 1)
	case uint8:
		return t & (t - 1)
	case uint16:
		return t & (t - 1)
	case uint32:
		return t & (t - 1)
	case uint64:
		return t & (t - 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   TurnOnRightmost 0 turns on the rightmost 0-bit in an int to 1.
*/
func TurnOnRightmost0(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t | (t + 1)
	case int8:
		return t | (t + 1)
	case int16:
		return t | (t + 1)
	case int32:
		return t | (t + 1)
	case int64:
		return t | (t + 1)
	case uint:
		return t | (t + 1)
	case uint8:
		return t | (t + 1)
	case uint16:
		return t | (t + 1)
	case uint32:
		return t | (t + 1)
	case uint64:
		return t | (t + 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   TurnOffTrailing1s turns off the rightmost string of 1's in an int to 0's.
*/
func TurnOffTrailing1s(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t & (t + 1)
	case int8:
		return t & (t + 1)
	case int16:
		return t & (t + 1)
	case int32:
		return t & (t + 1)
	case int64:
		return t & (t + 1)
	case uint:
		return t & (t + 1)
	case uint8:
		return t & (t + 1)
	case uint16:
		return t & (t + 1)
	case uint32:
		return t & (t + 1)
	case uint64:
		return t & (t + 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   TurnOnTrailing0s turns on the rightmost string of 0's in an int to 1's.
*/
func TurnOnTrailing0s(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t | (t - 1)
	case int8:
		return t | (t - 1)
	case int16:
		return t | (t - 1)
	case int32:
		return t | (t - 1)
	case int64:
		return t | (t - 1)
	case uint:
		return t | (t - 1)
	case uint8:
		return t | (t - 1)
	case uint16:
		return t | (t - 1)
	case uint32:
		return t | (t - 1)
	case uint64:
		return t | (t - 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   OnlyRightmost0to1 returns an int with the rightmost 0-bit turned on to 1, and
   all other bits turned off to 0.
*/
func OnlyRightmost0to1(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return ^t & (t + 1)
	case int8:
		return ^t & (t + 1)
	case int16:
		return ^t & (t + 1)
	case int32:
		return ^t & (t + 1)
	case int64:
		return ^t & (t + 1)
	case uint:
		return ^t & (t + 1)
	case uint8:
		return ^t & (t + 1)
	case uint16:
		return ^t & (t + 1)
	case uint32:
		return ^t & (t + 1)
	case uint64:
		return ^t & (t + 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   OnlyRightmost1to0 returns an int with the rightmost 1-bit turned off to 0,
   and all other bits turned to 1.
*/
func OnlyRightmost1to0(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return ^t | (t - 1)
	case int8:
		return ^t | (t - 1)
	case int16:
		return ^t | (t - 1)
	case int32:
		return ^t | (t - 1)
	case int64:
		return ^t | (t - 1)
	case uint:
		return ^t | (t - 1)
	case uint8:
		return ^t | (t - 1)
	case uint16:
		return ^t | (t - 1)
	case uint32:
		return ^t | (t - 1)
	case uint64:
		return ^t | (t - 1)

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   Trailing0sOnElseOff returns a signed int with 1's at the position of any
   trailing 0's in x, and 0's elsewhere.
   May also be written as:

   ^(x | -(x))

   or:

   (x & ^x) - 1
*/
func Trailing0sOnElseOff(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return ^t & (t - 1)
	case int8:
		return ^t & (t - 1)
	case int16:
		return ^t & (t - 1)
	case int32:
		return ^t & (t - 1)
	case int64:
		return ^t & (t - 1)
	case uint:
		return ^t & (t - 1)
	case uint8:
		return ^t & (t - 1)
	case uint16:
		return ^t & (t - 1)
	case uint32:
		return ^t & (t - 1)
	case uint64:
		return ^t & (t - 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   Trailing1sOffElseOn returns a signed int with 0's at the position of any
   trailing 1's in x, and 1's elsewhere.
*/
func Trailing1sOffElseOn(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return ^t | (t + 1)
	case int8:
		return ^t | (t + 1)
	case int16:
		return ^t | (t + 1)
	case int32:
		return ^t | (t + 1)
	case int64:
		return ^t | (t + 1)
	case uint:
		return ^t | (t + 1)
	case uint8:
		return ^t | (t + 1)
	case uint16:
		return ^t | (t + 1)
	case uint32:
		return ^t | (t + 1)
	case uint64:
		return ^t | (t + 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   IsolateRightmost1 returns a signed int with all but the rightmost 1's in x
   turned off to 0.
*/
func IsolateRightmost1(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t & -(t)
	case int8:
		return t & -(t)
	case int16:
		return t & -(t)
	case int32:
		return t & -(t)
	case int64:
		return t & -(t)
	//case uint, uint8, uint16, uint32, uint64:
	//requires signed int
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}

/*
   TurnOnRightmost1AndTrailing0s returns an int with the trailing string of 0's
   and the rightmost 1-bit on to 1, and off otherwise. If there are no trailing
   0's, return the int 1.
*/
func TurnOnRightmost1AndTrailing0s(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t ^ (t - 1)
	case int8:
		return t ^ (t - 1)
	case int16:
		return t ^ (t - 1)
	case int32:
		return t ^ (t - 1)
	case int64:
		return t ^ (t - 1)
	case uint:
		return t ^ (t - 1)
	case uint8:
		return t ^ (t - 1)
	case uint16:
		return t ^ (t - 1)
	case uint32:
		return t ^ (t - 1)
	case uint64:
		return t ^ (t - 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   TurnOffRightmost0AndTrailing1s returns an int with the trailing string of
   1's and the rightmost 0-bit off, and on otherwise. If there are no trailing
   1's, return the int 1.
*/
func TurnOffRightmost0AndTrailing1s(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return t ^ (t + 1)
	case int8:
		return t ^ (t + 1)
	case int16:
		return t ^ (t + 1)
	case int32:
		return t ^ (t + 1)
	case int64:
		return t ^ (t + 1)
	case uint:
		return t ^ (t + 1)
	case uint8:
		return t ^ (t + 1)
	case uint16:
		return t ^ (t + 1)
	case uint32:
		return t ^ (t + 1)
	case uint64:
		return t ^ (t + 1)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   TurnOffRightmost1s turns off the rightmost string of 1's to 0's.
*/
func TurnOffRightmost1s(x interface{}) interface{} {

	switch t := x.(type) {
	case int:
		return ((t | (t - 1)) + 1) & t
	case int8:
		return ((t | (t - 1)) + 1) & t
	case int16:
		return ((t | (t - 1)) + 1) & t
	case int32:
		return ((t | (t - 1)) + 1) & t
	case int64:
		return ((t | (t - 1)) + 1) & t
	case uint:
		return ((t | (t - 1)) + 1) & t
	case uint8:
		return ((t | (t - 1)) + 1) & t
	case uint16:
		return ((t | (t - 1)) + 1) & t
	case uint32:
		return ((t | (t - 1)) + 1) & t
	case uint64:
		return ((t | (t - 1)) + 1) & t
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   FindNextWithSame1Bits finds the smallest unsigned integer larger than x that
   has the same number of 1-bits as x.
*/
func FindNextWithSame1bits(x interface{}) interface{} {

	switch t := x.(type) {
	//case int, int8, int16, int32, int64:
	//requires unsigned int
	case uint:
		isolate := IsolateRightmost1(int(t)).(uint)
		sum := isolate + t
		return sum | (((t ^ sum) >> 2) / isolate)
	case uint8:
		isolate := IsolateRightmost1(int8(t)).(uint8)
		sum := isolate + t
		return sum | (((t ^ sum) >> 2) / isolate)
	case uint16:
		isolate := IsolateRightmost1(int16(t)).(uint16)
		sum := isolate + t
		return sum | (((t ^ sum) >> 2) / isolate)
	case uint32:
		isolate := IsolateRightmost1(int32(t)).(uint32)
		sum := isolate + t
		return sum | (((t ^ sum) >> 2) / isolate)
	case uint64:
		isolate := IsolateRightmost1(int64(t)).(uint64)
		sum := isolate + t
		return sum | (((t ^ sum) >> 2) / isolate)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))

	}
}
*/
