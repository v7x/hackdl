package util

import (
	"math/bits"
)

/*
   IntSize and IntShift are declared for use when shifting based on the bit
   width of an integer.
*/

const IntSize = bits.UintSize
const IntShift = IntSize - 1
