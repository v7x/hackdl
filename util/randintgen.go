package util

import (
	"math/rand"
)

/*
   RandArray and its derivatives generate an array slice of length len of
   random integers of that type using math/rand. Len is a uint.

   Useful for generating values for testing.
*/

func RandArray(len uint) []int {
	arr := make([]int, len)

	for i := range arr {
		if i%2 == 0 {
			arr[i] = rand.Int()
		} else {
			arr[i] = -(rand.Int())
		}
	}

	return arr
}

func RandArray8(len uint) []int8 {
	arr := make([]int8, len)

	for i := range arr {
		if i%2 == 0 {
			arr[i] = int8(rand.Intn(127))
		} else {
			arr[i] = int8(-(rand.Intn(128)))
		}
	}

	return arr
}

func RandArray16(len uint) []int16 {
	arr := make([]int16, len)

	for i := range arr {
		if i%2 == 0 {
			arr[i] = int16(rand.Intn(32767))
		} else {
			arr[i] = int16(-(rand.Intn(32768)))
		}
	}

	return arr
}

func RandArray32(len uint) []int32 {
	arr := make([]int32, len)

	for i := range arr {
		if i%2 == 0 {
			arr[i] = rand.Int31()
		} else {
			arr[i] = -(rand.Int31())
		}
	}

	return arr
}

func RandArray64(len uint) []int64 {
	arr := make([]int64, len)

	for i := range arr {
		if i%2 == 0 {
			arr[i] = rand.Int63()
		} else {
			arr[i] = -(rand.Int63())
		}
	}

	return arr
}

func RandArrayU(len uint) []uint {
	arr := make([]uint, len)

	for i := range arr {
		arr[i] = uint(rand.Int())
	}

	return arr
}

func RandArrayU8(len uint) []uint8 {
	arr := make([]uint8, len)

	for i := range arr {
		arr[i] = uint8(rand.Intn(255))
	}

	return arr
}

func RandArrayU16(len uint) []uint16 {
	arr := make([]uint16, len)

	for i := range arr {
		arr[i] = uint16(rand.Intn(65536))
	}

	return arr
}

func RandArrayU32(len uint) []uint32 {
	arr := make([]uint32, len)

	for i := range arr {
		arr[i] = rand.Uint32()
	}

	return arr
}

func RandArrayU64(len uint) []uint64 {
	arr := make([]uint64, len)

	for i := range arr {
		arr[i] = rand.Uint64()
	}

	return arr
}
