package util

import (
	"fmt"
)

/*
   IntBool returns false if x is 0, and true otherwise.
*/

func BoolInt(x interface{}) bool {
	switch x.(type) {
	case int, int8, int16, int32, int64:
		b := true
		if x == 0 {
			b = false
		}
		return b
	case uint, uint8, uint16, uint32, uint64:
		b := true
		if x == 0 {
			b = false
		}
		return b
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   IntBool returns 1 if x, and 0 otherwise.
*/
func IntBool(x bool) int {
	b := 0
	if x {
		b = 1
	}
	return b
}

/*
func x(x interface{}) interface{} {

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	t1 := x.(type)
	t2, ok := y.(t1)

	if !(ok) {
		panic(fmt.Sprintf("%T is not %T.", y, x))
	}

	switch t := x.(type) {
	case int, int8, int16, int32, int64:

	case uint, uint8, uint16, uint32, uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
