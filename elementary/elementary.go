package elementary

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
)

/*
   Sqrt finds an integer's square root using newton's method.
*/
func Sqrt(t interface{}) interface{} {
	switch x := t.(type) {
	case uint:
		if util.IntSize == 32 {
			return Sqrt(uint32(x))
		} else {
			return Sqrt(uint64(x))
		}
	case uint8:
		if x <= 1 {
			return x
		}

		s := 1
		x1 := x - 1

		if x1 > 15 {
			s += 2
			x >>= 4
		}

		if x > 3 {
			s += 1
		}

		g0 := 1 << s               // 2 to the s power
		g1 := (g0 + (x >> s)) >> 1 // g1 = (g0 + x/g0)/2

		for g1 < g0 {
			g0 = g1
			g1 = (g0 + (x / g0)) >> 1
		}
	case uint16:
		if x <= 1 {
			return x
		}

		s := 1
		x1 := x - 1

		if x1 > 255 {
			s += 4
			x1 >>= 8
		}

		if x1 > 15 {
			s += 2
			x >>= 4
		}

		if x > 3 {
			s += 1
		}

		g0 := 1 << s               // 2 to the s power
		g1 := (g0 + (x >> s)) >> 1 // g1 = (g0 + x/g0)/2

		for g1 < g0 {
			g0 = g1
			g1 = (g0 + (x / g0)) >> 1
		}
	case uint32:
		if x <= 1 {
			return x
		}

		s := 1
		x1 := x - 1

		if x1 > 65535 {
			s += 8
			x1 >>= 16
		}

		if x1 > 255 {
			s += 4
			x1 >>= 8
		}

		if x1 > 15 {
			s += 2
			x >>= 4
		}

		if x > 3 {
			s += 1
		}

		g0 := 1 << s               // 2 to the s power
		g1 := (g0 + (x >> s)) >> 1 // g1 = (g0 + x/g0)/2

		for g1 < g0 {
			g0 = g1
			g1 = (g0 + (x / g0)) >> 1
		}
		return g0

	case uint64:
		if x <= 1 {
			return x
		}

		s := 1
		x1 := x - 1

		if x1 > 2147483647 {
			s += 16
			x1 >>= 32
		}

		if x1 > 65535 {
			s += 8
			x1 >>= 16
		}

		if x1 > 255 {
			s += 4
			x1 >>= 8
		}

		if x1 > 15 {
			s += 2
			x >>= 4
		}

		if x > 3 {
			s += 1
		}

		g0 := 1 << s               // 2 to the s power
		g1 := (g0 + (x >> s)) >> 1 // g1 = (g0 + x/g0)/2

		for g1 < g0 {
			g0 = g1
			g1 = (g0 + (x / g0)) >> 1
		}
		return g0
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
   Cbrt calculates the cube root of x.
*/
func Cbrt(x int32) int32 {
	y := 0

	for s := 30; s >= 0; s -= 3 {
		y *= 2
		b := (3*y*(y+1) + 1) << s
		if x >= b {
			x -= b
			y++
		}
	}
	return y
}

/*
   Exponent calculates x to the power of n.
*/
func Exponent(x int, n uint) int {
	y := 1
	p := x

	for true {
		if util.IntBool(n & 1) {
			y = p * y
		}

		n >>= 1
		if n == 0 {
			return y
		}
		p *= p
	}
}

/*
   FortranPow2 calculates 2 to the power of n in the same way Fortran does,
   returning 2**n when 0 <= n <= 30, -2**31 when n == 31, and 0 when n < 0 or
   n >= 32.
*/
func FortranPow2(n int32) int32 {
	x := bitcount.NLZ(uint32(n) >> 5).(int32)
	x = uint32(x) >> 5
	return x << n
}

func Log2(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log2(uint(t))
	case int8:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log2(uint8(t))
	case int16:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log2(uint16(t))
	case int32:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log2(uint32(t))
	case int64:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log2(uint64(t))
	case uint:
		if util.IntSize == 32 {
			return Log2(uint32(t))
		} else {
			return Log2(uint64(t))
		}
	case uint8:
		f := bitcount.PopCount(t).(int8)
		f |= (f >> 1)
		f |= (f >> 2)
		f |= (f >> 4)
		return bitcount.PopCount(x) - 1
	case uint16:
		f := bitcount.PopCount(t).(int16)
		f |= (f >> 1)
		f |= (f >> 2)
		f |= (f >> 4)
		f |= (f >> 8)
		return bitcount.PopCount(x) - 1
	case uint32:
		f := bitcount.PopCount(t).(int32)
		f |= (f >> 1)
		f |= (f >> 2)
		f |= (f >> 4)
		f |= (f >> 8)
		f |= (f >> 16)
		return bitcount.PopCount(x) - 1
	case uint64:
		f := bitcount.PopCount(t).(int64)
		f |= (f >> 1)
		f |= (f >> 2)
		f |= (f >> 4)
		f |= (f >> 8)
		f |= (f >> 16)
		f |= (f >> 32)
		return bitcount.PopCount(x) - 1
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func Log10(x interface{}) interface{} {
	switch t := x.(type) {
	case int:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log10(uint(t))
	case int8:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log10(uint8(t))
	case int16:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log10(uint16(t))
	case int32:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log10(uint32(t))
	case int64:
		if t < 0 {
			panic("Logarithm of a negative is undefined.")
		}
		return Log10(uint64(t))
	case uint:
		if util.IntSize == 32 {
			return Log10(uint32(t))
		} else {
			return Log10(uint64(t))
		}
	case uint8:
		return Log10(uint32(t))
	case uint16:
		return Log10(uint32(t))
	case uint32:
		table := [11]uint32{
			0,
			9,
			99,
			999,
			9999,
			99999,
			999999,
			9999999,
			99999999,
			999999999,
			0xFFFFFFFF,
		}

		y := (19 * (31 - bitcount.NLZ(t).(uint32))) >> 6
		y += ((table[y+1] - t) >> 31)
		return y
	case uint64:
		table := [20]uint64{
			0,
			9,
			99,
			999,
			9999,
			99999,
			999999,
			9999999,
			99999999,
			999999999,
			9999999999,
			99999999999,
			999999999999,
			9999999999999,
			99999999999999,
			999999999999999,
			9999999999999999,
			99999999999999999,
			999999999999999999,
			9999999999999999999,
		}

		y = (19 * (63 - bitcount.NLZ(t).(uint64))) >> 6
		y += (table[y+1] - t) >> 63
		return y
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(int)
        case int8:
		u := y.(int8)
        case int16:
		u := y.(int16)
        case int32:
		u := y.(int32)
        case int64:
		u := y.(int64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
