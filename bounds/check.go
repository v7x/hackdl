package bounds

import (
	"fmt"
	"gitlab.com/v7x/hackdl/util"
)

/*
 */
func Within(x, upper, lower interface{}) bool {
	if util.GT(upper, lower).(int64) != 1 {
		panic("Upper bound is less than lower bound.")
	}
	switch t := x.(type) {
	case int:
		u, w := upper.(int), lower.(int)
		return t-w <= u-w
	case int8:
		u, w := upper.(int8), lower.(int8)
		return t-w <= u-w
	case int16:
		u, w := upper.(int16), lower.(int16)
		return t-w <= u-w
	case int32:
		u, w := upper.(int32), lower.(int32)
		return t-w <= u-w
	case int64:
		u, w := upper.(int64), lower.(int64)
		return t-w <= u-w
	case uint:
		u, w := upper.(uint), lower.(uint)
		return t-w <= u-w
	case uint8:
		u, w := upper.(uint8), lower.(uint8)
		return t-w <= u-w
	case uint16:
		u, w := upper.(uint16), lower.(uint16)
		return t-w <= u-w
	case uint32:
		u, w := upper.(uint32), lower.(uint32)
		return t-w <= u-w
	case uint64:
		u, w := upper.(uint64), lower.(uint64)
		return t-w <= u-w
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

/*
func x(x interface{}) interface{} {
	switch t := x.(type) {
        case int:

        case int8:

        case int16:

        case int32:

        case int64:

        case uint:

        case uint8:

        case uint16:

        case uint32:

        case uint64:

	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}

func x(x, y interface{}) interface{} {
	switch t := x.(type) {
        case int:
		u := y.(uint)
        case int8:
		u := y.(uint8)
        case int16:
		u := y.(uint16)
        case int32:
		u := y.(uint32)
        case int64:
		u := y.(uint64)
        case uint:
		u := y.(uint)
        case uint8:
		u := y.(uint8)
        case uint16:
		u := y.(uint16)
        case uint32:
		u := y.(uint32)
        case uint64:
		u := y.(uint64)
	default:
		panic(fmt.Sprintf("Unexpected type: %T", x))
	}
}
*/
